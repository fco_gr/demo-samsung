<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('user', API\V1\UserController::class)->middleware('auth:api');

Route::middleware('auth:api')->group(function () {
    Route::post('dashboard', 'API\V1\DashboardController@getResumen');
    Route::post('dashboard_total', 'API\V1\DashboardController@getTotales');
    Route::post('punto_contacto', 'API\V1\PuntoContactoController@getDatos');
    Route::post('evaluaciones', 'API\V1\EvaluacionController@getDatos');
    Route::post('evaluacion', 'API\V1\EvaluacionController@show');
    Route::post('ranking_subgerente', 'API\V1\RankingSubgerenteController@getRanking');
    Route::post('ranking_jefe_zona', 'API\V1\RankingJefeZonaController@getRanking');
    Route::post('ranking_plataforma', 'API\V1\RankingPlataformaController@getRanking');
    Route::post('ranking_file', 'API\V1\RankingFileController@getRanking');
    Route::post('biblioteca', 'API\V1\BibliotecaController@getDatos');


    Route::get('doit/estudios', 'API\V1\SyncController@getEstudios');
    
    Route::post('sync', 'API\V1\SyncController@copiaDatosDoit')->name('sync.copia_datos');
    Route::post('pivot', 'API\V1\DatoConsolidadoController@pivot')->name('pivot');

    Route::get('resumen_datos_consolidados', 'API\V1\DatoConsolidadoController@getResumenCodEstudio')->name('getResumenCodEstudio');
    
    Route::get('mes_inicial', function(){
        return mesInicial();
    });

    Route::get('/file/list', 'API\V1\VariableFiltroController@listFiles');


    // Route::post('ranking_local', 'API\V1\RankingLocalController@getRanking');
    // Route::post('benchmark', 'API\V1\BenchmarkController@index');
    
    // Route::post('variedad_por_local_producto', 'API\V1\VariedadPorLocalController@getPreguntas');

    // Route::get('medicion/list/', 'API\V1\MedicionController@list');
    
    // Route::post('evaluacion_list', 'API\V1\EvaluacionController@getListByMedicion');
    // Route::post('evaluacion', 'API\V1\EvaluacionController@getSucursalByCodMedicion');
    
    // Route::post('descarga_db', 'API\V1\BaseDeDatosController@DescargaBase');

    

});



