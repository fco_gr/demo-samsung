<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dashboard', 'DashboardController')->name('dashboard.index');
    Route::get('biblioteca', 'BibliotecaController')->name('biblioteca.index');
    Route::get('punto_contacto', 'PuntoContactoController')->name('punto_contacto.index');
    Route::get('evaluacion', 'EvaluacionController')->name('evaluacion.index');
    Route::get('ranking_subgerente', 'RankingSubgerenteController')->name('ranking.subgerente.index');
    Route::get('ranking_jefe_zona', 'RankingJefeZonaController')->name('ranking.jefe_zona.index');
    Route::get('ranking_plataforma', 'RankingPlataformaController')->name('ranking.plataforma.index');
    Route::get('ranking_file', 'RankingFileController')->name('ranking.file.index');
    Route::get('sabana', 'SabanaController@index')->name('sabana.index');
    Route::get('sabana/{medicion}/{pauta}', 'SabanaController@export')->name('sabana.index.recupera');
    
    // Solo se una para crear files masivos
    // Route::get('crea_file', 'CreaFileController')->name('ranking.file.index');

    Route::get('sync', 'SyncController@index')->name('sync.index');
    Route::get('pivot', 'PivotController@index')->name('pivot.index');

    Route::get('eds/import', 'EdsImportController@show')->name('eds.import.show');
    Route::post('eds/import', 'EdsImportController@store')->name('eds.import.store');
});

Route::get('users', 'UserController')->name('user.index');
Auth::routes(['register' => false]);




