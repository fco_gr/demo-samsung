<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicions', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->string('descripcion_corto');
            $table->integer('agno');
            $table->integer('mes');
            $table->string('trimestre', 50);
            $table->integer('fecha_numero');
            $table->boolean('activo')->defaul(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicions');
    }
}
