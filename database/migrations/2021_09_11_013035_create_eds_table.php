<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eds', function (Blueprint $table) {
            $table->id();
            $table->string('razon_social')->nullable();
            $table->string('rut_rz')->nullable();
            $table->string('direccion')->nullable();
            $table->string('comuna')->nullable();
            $table->string('region')->nullable();
            $table->string('jefe_zona')->nullable();
            $table->string('subgerente_area')->nullable();
            $table->string('plataforma')->nullable();
            $table->string('j_zona_tienda')->nullable();
            $table->string('jefe_lavado')->nullable();
            $table->string('s_zona_tienda')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eds');
    }
}
