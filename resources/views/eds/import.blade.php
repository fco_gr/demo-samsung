@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="far fa-file-alt"></i>
    Import Eds - Listado de sitios XLS
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Import Excel</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($errors) && $errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif

                    @if (session()->has('failures'))

                        <table class="table table-danger">
                            <tr>
                                <th>Row</th>
                                <th>Attribute</th>
                                <th>Errors</th>
                                <th>Value</th>
                            </tr>

                            @foreach (session()->get('failures') as $validation)
                                <tr>
                                    <td>{{ $validation->row() }}</td>
                                    <td>{{ $validation->attribute() }}</td>
                                    <td>
                                        <ul>
                                            @foreach ($validation->errors() as $e)
                                                <li>{{ $e }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        {{ $validation->values()[$validation->attribute()] }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    @endif

                    <form action="/eds/import" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group mt-3">
                            <select name="pauta" class="form-select" aria-label="Default select example">
                                <option selected disabled>Seleccionar Pauta</option>
                                <option value="1">Combustible</option>
                                <option value="2">Tienda</option>
                                <option value="3">Lavado</option>
                            </select>
                            <input class="form-select mt-3" type="file" name="file" />
                            <button type="submit" class="btn btn-primary mt-3">Import</button>
                        </div>
                    </form>
                </button>
            </div>
        </div>
    </div>
</div>
    
@endsection