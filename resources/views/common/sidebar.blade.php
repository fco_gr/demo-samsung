<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                {{-- <div class="sb-sidenav-menu-heading">Core</div> --}}
                <a class="{{ request()->is('dashboard') ? "nav-link active" : "nav-link" }}"  href="{{ route('dashboard.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    Dashboard
                </a>
                <a class="{{ request()->is('punto_contacto') ? "nav-link active" : "nav-link" }}" href="{{ route('punto_contacto.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-book"></i>
                    </div>
                    Punto de Contacto
                </a>
                
                <a class="{{ request()->is('evaluacion') ? "nav-link active" : "nav-link" }}" href="{{ route('evaluacion.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="far fa-file-alt"></i>
                    </div>
                    Evaluaciones
                </a>

                @cannot('isFile')
                <a class="{{ request()->is('ranking_*') ? "nav-link" : "nav-link collapsed" }}" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="{{ request()->is('ranking_*') ? "true" : "false" }}" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-down"></i></div>
                    Ranking
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="{{ request()->is('ranking_*') ? "collapse show" : "collapse" }}" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="{{ request()->is('ranking_subgerente') ? "nav-link active" : "nav-link" }}" href="{{ route('ranking.subgerente.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-down"></i></div>
                            Subgerente
                        </a>
                        <a class="{{ request()->is('ranking_jefe_zona') ? "nav-link active" : "nav-link" }}" href="{{ route('ranking.jefe_zona.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-down"></i></div>
                            Jefe Zona
                        </a>
                        <a class="{{ request()->is('ranking_plataforma') ? "nav-link active" : "nav-link" }}" href="{{ route('ranking.plataforma.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-down"></i></div>
                            Platafoma
                        </a>
                        <a class="{{ request()->is('ranking_file') ? "nav-link active" : "nav-link" }}" href="{{ route('ranking.file.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-down"></i></div>
                            Tienda
                        </a>
                    </nav>
                </div>
                @endcannot
                
                @can('isSabana')
                    <a class="{{ request()->is('sabana') ? "nav-link active" : "nav-link" }}" href="{{ route('sabana.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="far fa-file-alt"></i>
                        </div>
                        Base de datos
                    </a>
                @endcan

                {{-- @canany(['isAdmin', 'isGerente']) --}}
                {{-- @can('isBiblioteca')
                    <a class="{{ request()->is('biblioteca') ? "nav-link active" : "nav-link" }}" href="{{ route('biblioteca.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-photo-video"></i>
                        </div>
                        Biblioteca
                    </a>
                @endcan --}}
                {{-- @endcanany --}}

                {{-- 
                <a class="{{ request()->is('satisfaccion_global') ? "nav-link active" : "nav-link" }}" href="{{ route('satisfaccion-global.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="far fa-thumbs-up"></i>
                    </div>
                    Satisfacción Global.
                </a>
                <a class="{{ request()->is('evolutivo') ? "nav-link active" : "nav-link" }}" href="{{ route('evolutivo.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-chart-line"></i>
                    </div>
                    Evolutivo
                </a>
                <a class="{{ request()->is('venta') ? "nav-link active" : "nav-link" }}" href="{{ route('venta.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    Venta.
                </a>
                <a class="{{ request()->is('acompanamiento') ? "nav-link active" : "nav-link" }}" href="{{ route('acompanamiento.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-user-friends"></i>
                    </div>
                    Acompañamiento.
                </a>
                <a class="{{ request()->is('visita_y_pago') ? "nav-link active" : "nav-link" }}" href="{{ route('visita_y_pago.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="far fa-credit-card"></i>
                    </div>
                    Visitas y Pago.
                </a>
                <a class="{{ request()->is('reclamo') ? "nav-link active" : "nav-link" }}" href="{{ route('reclamo.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-exclamation-triangle"></i>
                    </div>
                    Reclamos.
                </a>
                <a class="{{ request()->is('comparativo_parques') ? "nav-link active" : "nav-link" }}" href="{{ route('comparativo-parques.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-compress-alt"></i>
                    </div>
                    Comparativo Parques.
                </a>
                <a class="nav-link" href="#">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-times-circle"></i>
                    </div>
                    Problemas
                </a>
                <a class="nav-link" href="#">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-bell"></i>
                    </div>
                    Alertas
                </a>
                <a class="nav-link" href="#">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-database"></i>
                    </div>
                    Descarga BD
                </a> --}}
                
                
                @can('isAdmin')
                    <div class="sb-sidenav-menu-heading">Administración</div>
                    <a class="{{ request()->is('users') ? "nav-link active" : "nav-link" }}" href="{{ route('user.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Usuarios
                    </a>

                    <a class="{{ request()->is('sync') ? "nav-link active" : "nav-link" }}" href="{{ route('sync.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Sincroniza
                    </a>
                    <a class="{{ request()->is('pivot') ? "nav-link active" : "nav-link" }}" href="{{ route('pivot.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Pivot
                    </a>
                    <a class="{{ request()->is('eds/import') ? "nav-link active" : "nav-link" }}" href="{{ route('eds.import.show') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Importa File
                    </a>
                    
                   
                    
                @endcan
            </div>
        </div>
        
    </nav>
</div>