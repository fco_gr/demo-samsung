@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="fas fa-photo-video"></i>
    Biblioteca
@endsection

@section('content')
<div class="row">
    <biblioteca></biblioteca>
</div>
    
@endsection