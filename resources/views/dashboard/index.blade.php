@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="far fa-file-alt"></i>
    Dashboard
@endsection

@section('content')
<div class="row">
    <dashboard></dashboard>
</div>
    
@endsection