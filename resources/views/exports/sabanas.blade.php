<?php
    function filtraRespuesta($valor){
        $valorFinal = $valor;
        if ($valorFinal == 99){
            $valorFinal = '';
        }
        else if ($valorFinal == 2){
            $valorFinal = 0;
        }

        return $valorFinal;
    }

?>


<table>
    <thead>
        <tr>
            <th style="background: #00ccff">COD_TAREA</th>
            <th style="background: #00ccff">SHOOPER</th>
            <th style="background: #00ccff">AÑO</th>
            <th style="background: #00ccff">MES</th>
            <th style="background: #00ccff">FILE</th>
            <th style="background: #00ccff">VISITA</th>
            <th style="background: #00ccff">PLATAFORMA</th>
            <th style="background: #00ccff">DIRECCION</th>
            <th style="background: #00ccff">COMUNA</th>
            <th style="background: #00ccff">REGION</th>
            <th style="background: #00ccff">JEFE DE ZONA</th>
            <th style="background: #00ccff">SUBGERENTE</th>
            <th style="background: #00ccff">NOMBRE COLABORADOR</th>
            <th style="background: #00ccff">FECHA</th>
            <th style="background: #00ccff">TRAMO DE HORA</th>
            
            <th style="background: #417FFC">NOTA1 Califique la actitud de amabilidad del atendedor(de 1 a 7)</th>
            <th style="background: #417FFC">P1_17 El atendedor se encontraba con su mascarilla, tapando boca y nariz</th>
            <th style="background: #417FFC">p1_17_1 Razón No cumplimiento P1_17</th>
            <th style="background: #417FFC">P1_22 ¿El atendedor le preguntó si deseaba boleta o factura?</th>

            <th style="background: #99cc00">RESULTADO_FINAL</th>
            <th style="background: #99cc00">PUNTAJE_OBTENIDO</th>
            <th style="background: #99cc00">PUNTAJE_TOTAL</th>

            <th style="background: #008000">PUNTAJE BIENVENIDA TOTAL</th>
            <th style="background: #008000">P1_2 Al acercarse al sitio, ¿el atendedor se encontraba en posición vigia, es decir, estaba en la punta de la isla en una actitud atenta? </th>
            <th style="background: #008000">P1_2_1 Razón No cumplimiento P1_2</th>
            <th style="background: #008000">P1_3 Al acercarse al sitio, ¿el atendedor lo dirigió con una seña a la posición de carga desocupada? </th>
            <th style="background: #008000">P1_3_1 Razón No cumplimiento P1_3</th>
            <th style="background: #008000">P1_5 ¿El atendedor saluda de forma cordial?</th>
            <th style="background: #ff9900">PUNTAJE RAPIDEZ TOTAL</th>
            <th style="background: #ff9900">P1_1 Al estar en la fila, en 2° posición, ¿le realizaron una señal de que será atendido prontamente? </th>
            <th style="background: #ff9900">P1_1_1  Razón No cumplimiento P1_1</th>
            <th style="background: #ff9900">P1_4 ¿Fue atendido rápidamente(antes de 45 segundos) por un atendedor en la playa?</th>
            <th style="background: #ff9900">P1_4_1 Razón No cumplimiento P1_4</th>
            <th style="background: #ff9900">P1_13 ¿La transacción de pago en el sector de carga se completó de manera rápida, correcta y eficiente?</th>
            <th style="background: #ff9900">P1_13_1 Razón No cumplimiento P1_13</th>
            <th style="background: #ffcc99">PUNTAJE CONFIANZA TOTAL</th>
            <th style="background: #ffcc99">P1_7 ¿El atendedor le indicó que el surtidor se encuentra en cero? NOTA: El atendedor debe asegurarse que el cliente mira el surtidor</th>
            <th style="background: #ffcc99">P1_9 ¿El atendedor le dio un resumen de la venta detallando monto de la venta y litros cargados? </th>
            <th style="background: #ffcc99">P1_9_1 Razón No cumplimiento P1_9</th>
            <th style="background: #ffcc99">P1_15 ¿El atendedor llevaba su piocha de identificación con su nombre claramente legible?</th>
            <th style="background: #ffcc99">P1_15_1 Razón No cumplimiento P1_15</th>
            <th style="background: #ffcc99">P1_16 ¿El atendedor llevaba uniforme limpio y autorizado por shell?</th>
            <th style="background: #ffcc99">P1_16_1 Razón No cumplimiento P1_16</th>

            <th style="background: #ffcc99">P1_21 ¿El rut dictado es el mismo que aparece en el resumen de la boleta?</th>
            <th style="background: #ffcc99">P1_21_1 Razón No cumplimiento P1_21</th>


            <th style="background: #ccffff">PUNTAJE ACTITUD TOTAL</th>
            <th style="background: #ccffff">P1_6 ¿El atendedor le ofreció cargar gasolina o diesel? </th>
            <th style="background: #ccffff">P1_6_1 Razón No cumplimiento P1_6</th>

            <th style="background: #ccffff">P1_8 ¿El atendedor le ofreció un servicio adicional? </th>
            <th style="background: #ccffff">P1_8_1 Indique cuál ofrece</th>
            <th style="background: #ccffff">P1_8_2 Indique NO cumplimiento P1_8</th>

            <th style="background: #ccffff">P1_11 Una vez terminada la carga,¿el atendedor le preguntó la forma de pago antes de emitir la boleta? </th>
            <th style="background: #ccffff">P1_11_1 Razón No cumplimiento P1_11</th>
            <th style="background: #ccffff">P1_14 ¿El atendedor agradece y se despide en forma cordial? </th>
            <th style="background: #ccffff">P1_12 ¿ El atendedor le preguntó si pagaba con MiCopiloto?</th>
            
            <th style="background: #ccffff">P1_19 ¿El atendedor ofreció la opción de acumular CMR puntos? </th>

            <th style="background: #ccffff">P1_18 Nombre Boleta</th>
            <th style="background: #ffff99">PUNTAJE_OBTENIDO_V2</th>
            <th style="background: #ffff99">META</th>
            <th style="background: #ffff99">RESULTADO_FINAL_V2</th>
            


        </tr>
    </thead>
    <tbody>
        @foreach($sabanas as $sabana)
            <tr>
                <td>{{ $sabana->cod_tarea }} </td>
                <td>{{ $sabana->cod_dooer }} </td>
                <td>{{ $sabana->agno }} </td>
                <td>{{ $sabana->mes }} </td>
                <td>{{ $sabana->id }} </td>
                <td>{{ $sabana->VISITA }}</td>
                <td>{{ $sabana->plataforma }} </td>
                <td>{{ $sabana->direccion }} </td>
                <td>{{ $sabana->comuna }} </td>
                <td>{{ $sabana->region }} </td>
                <td>{{ $sabana->jefe_zona }} </td>
                <td>{{ $sabana->subgerente_area }} </td>
                <td>{{ $sabana->nom_colaborador }} </td>
                <td>{{ $sabana->fecha_tramo }} </td>
                <td>{{ $sabana->horallegada_tramo }} </td>

                <td>{{ $sabana->NOTA1 }} </td>
                <td>{{ filtraRespuesta($sabana->P1_17) }} </td>
                <td>{{ $sabana->P1_17_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_22) }} </td>

                <td>{{ $sabana->resultado_final / 100 }} </td>
                <td>{{ $sabana->puntaje_obtenido }} </td>
                <td>{{ $sabana->puntaje_total }} </td>
                <td>{{ $sabana->puntaje_bienvenida_total / 100 }} </td>

                <td>{{ filtraRespuesta($sabana->P1_2) }} </td>
                <td>{{ $sabana->P1_2_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_3) }} </td>
                <td>{{ $sabana->P1_3_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_5) }} </td>

                <td>{{ $sabana->puntaje_rapidez_total / 100}} </td>
                <td>{{ filtraRespuesta($sabana->P1_1) }} </td>
                <td>{{ $sabana->P1_1_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_4) }} </td>
                <td>{{ $sabana->P1_4_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_13) }} </td>
                <td>{{ $sabana->P1_13_1_detalle }} </td>

                <td>{{ $sabana->puntaje_confianza_total / 100 }} </td>
                <td>{{ filtraRespuesta($sabana->P1_7) }} </td>
                
                <td>{{ filtraRespuesta($sabana->P1_9) }} </td>
                <td>{{ $sabana->P1_9_1_detalle }} </td>



                <td>{{ filtraRespuesta($sabana->P1_15) }} </td>
                <td>{{ $sabana->P1_15_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_16) }} </td>
                <td>{{ $sabana->P1_16_1_detalle }} </td>

                <td>{{ filtraRespuesta($sabana->P1_21) }} </td>
                <td>{{ $sabana->P1_21_1_detalle }} </td>

                <td>{{ $sabana->puntaje_actitud_total / 100 }} </td>
                <td>{{ filtraRespuesta($sabana->P1_6) }} </td>
                <td>{{ $sabana->P1_6_1_detalle }} </td>

                <td>{{ filtraRespuesta($sabana->P1_8) }} </td>
                <td>{{ $sabana->P1_8_1_detalle }} </td>
                <td>{{ $sabana->P1_8_2_detalle }} </td>

                <td>{{ filtraRespuesta($sabana->P1_11) }} </td>
                <td>{{ $sabana->P1_11_1_detalle }} </td>
                <td>{{ filtraRespuesta($sabana->P1_14) }} </td>
                
                <td>{{ filtraRespuesta($sabana->P1_12) }} </td>

                <td>{{ filtraRespuesta($sabana->P1_19) }} </td>


                <td>{{ $sabana->P1_18 }} </td>

                <td>{{ $sabana->resultado_final / 100 }} </td>
                <td>{{ $sabana->meta_final }} </td>
                <td>{{ $sabana->resultado_final_v2 / 100 }} </td>

            </tr>
        @endforeach
    </tbody>
</table>