<?php
    function filtraRespuesta($valor){
        $valorFinal = $valor;
        if ($valorFinal == 99){
            $valorFinal = '';
        }
        else if ($valorFinal == 2){
            $valorFinal = 0;
        }

        return $valorFinal;
    }

?>


<table>
    <thead>
        <tr>
            <th style="background: #00ccff">COD_TAREA</th>
            <th style="background: #00ccff">SHOPER</th>
            <th style="background: #00ccff">AÑO</th>
            <th style="background: #00ccff">MES</th>
            <th style="background: #00ccff">FILE</th>
            <th style="background: #00ccff">VISITA</th>
            <th style="background: #00ccff">PLATAFORMA</th>
            <th style="background: #00ccff">DIRECCION</th>
            <th style="background: #00ccff">COMUNA</th>
            <th style="background: #00ccff">REGION</th>
            <th style="background: #00ccff">JEFE DE ZONA</th>
            <th style="background: #00ccff">SUBGERENTE</th>
            <th style="background: #00ccff">NOMBRE COLABORADOR</th>
            <th style="background: #00ccff">FECHA</th>
            <th style="background: #00ccff">TRAMO DE HORA</th>

            <th style="background: #417FFC">NOTA3 Califique la actitud de amabilidad del atendedor(de 1 a 7)</th>
            <th style="background: #417FFC">P2_15 El lavador se encontraba con su mascarilla, tapando boca y nariz</th>
            <th style="background: #417FFC">P2_15_1 Razón No cumplimiento P2_15_1</th>


            <th style="background: #99cc00">RESULTADO_FINAL</th>
            <th style="background: #99cc00">PUNTAJE_OBTENIDO</th>
            <th style="background: #99cc00">PUNTAJE_TOTAL</th>

            <th style="background: #008000">PUNTAJE BIENVENIDA TOTAL</th>
            <th style="background: #008000">P3_3 ¿El lavador saluda de forma cordial? </th>

            <th style="background: #ff9900">PUNTAJE RAPIDEZ TOTAL</th>
            <th style="background: #ff9900">P3_2 Al llegar a la fila del lavado(en 2° posición) , ¿el lavador me realiza alguna seña indicando que será atendido prontamente?</th>
            <th style="background: #ff9900">P3_1 Al llegar al lavado, ¿había un lavador de punto fijo en la entrada del lavado? </th>
            
            <th style="background: #ffcc99">PUNTAJE CONFIANZA TOTAL</th>
            <th style="background: #ffcc99">P3_5 ¿El lavador preguntó qué tipo de lavado va a usar y explica diferencias?</th>
            <th style="background: #ffcc99">P3_5_1 Razón No cumplimiento P3_5</th>
            <th style="background: #ffcc99">P3_7 ¿El Lavador le entrega el voucher con el tipo de lavado solicitado?</th>
            <th style="background: #ffcc99">P3_9 ¿El Lavador le indica como ingresar a la nave de lavado? </th>
            <th style="background: #ffcc99">P3_10 ¿Había un Lavador en la salida de la máquina de lavado y le indica cuando salir de la nave de lavado? </th>
            <th style="background: #ffcc99">P3_13 ¿El Lavador, llevaba su piocha de identificación autorizada por Enex?</th>
            <th style="background: #ffcc99">P3_13_1 Razón No cumplimiento P3_13</th>
            <th style="background: #ffcc99">P3_14 ¿El lavador llevaba el uniforme completo autorizado?</th>
            <th style="background: #ffcc99">P3_14_1 Razón No cumplimiento P3_14</th>

            <th style="background: #ccffff">PUNTAJE ACTITUD TOTAL</th>
            <th style="background: #ccffff">P3_6 ¿El Lavador pregunta la forma de pago?</th>
            <th style="background: #ccffff">P3_8 ¿Lavador realiza pre lavado de la carrocería en no más de 5 minutos?</th>
            <th style="background: #ccffff">P3_11 ¿El lavador realiza las terminaciones de secado manual?</th>
            <th style="background: #ccffff">P3_12 ¿El Lavador agradece y se despide en forma cordial? </th>
            <th style="background: #ccffff">P3_15 ¿El lavador se encontraba con su mascarilla, tapando boca y nariz?</th>
            <th style="background: #ccffff">P3_15_1 Razón No cumplimiento P3_15</th>
            <th style="background: #ccffff">P3_17 ¿El lavador pregunta si desea acumular CMR puntos?</th>
            <th style="background: #ffff99">PUNTAJE_OBTENIDO_V2</th>
            <th style="background: #ffff99">META</th>
            <th style="background: #ffff99">RESULTADO_FINAL_V2</th>
            

            
            


            
            


        </tr>
    </thead>
    <tbody>
        @foreach($sabanas as $sabana)
            <tr>
                <td>{{  $sabana->cod_tarea }} </td>
                <td>{{ $sabana->cod_dooer }} </td>
                <td>{{  $sabana->agno }} </td>
                <td>{{  $sabana->mes }} </td>
                <td>{{  $sabana->id }} </td>
                {{-- <td>{{  $sabana->descripcion }} </td> --}}
                <td>{{ $sabana->VISITA }}</td>
                <td>{{  $sabana->plataforma }} </td>
                <td>{{  $sabana->direccion }} </td>
                <td>{{  $sabana->comuna }} </td>
                <td>{{  $sabana->region }} </td>
                <td>{{  $sabana->jefe_zona }} </td>
                <td>{{  $sabana->subgerente_area }} </td>
                <td>{{  $sabana->nom_colaborador }} </td>
                <td>{{ $sabana->fecha_tramo }} </td>
                <td>{{ $sabana->horallegada_tramo }} </td>

                <td>{{  $sabana->NOTA3 }}
                <td>{{  filtraRespuesta($sabana->P3_15) }}
                <td>{{  $sabana->P3_15_1_detalle }}

                <td>{{  $sabana->resultado_final / 100 }} </td>
                <td>{{  $sabana->puntaje_obtenido }} </td>
                <td>{{  $sabana->puntaje_total }} </td>

                <td>{{  $sabana->puntaje_bienvenida_total / 100 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_3) }} </td>
                
                <td>{{  $sabana->puntaje_rapidez_total / 100 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_2) }} </td>
                <td>{{  $sabana->P3_1 }} </td>
                <td>{{  $sabana->puntaje_confianza_total / 100 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_5) }} </td>
                <td>{{  $sabana->P3_5_1 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_7) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_9) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_10) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_13) }} </td>
                <td>{{  $sabana->P3_13_1 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_14) }} </td>
                <td>{{  $sabana->P3_14_1 }} </td>

                <td>{{  $sabana->puntaje_actitud_total / 100 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_6) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_8) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_11) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_12) }} </td>
                <td>{{  filtraRespuesta($sabana->P3_15) }} </td>
                <td>{{  $sabana->P3_15_1 }} </td>
                <td>{{  filtraRespuesta($sabana->P3_17) }} </td>

                <td>{{  $sabana->resultado_final / 100 }} </td>
                <td>{{  $sabana->meta_final }} </td>
                <td>{{  $sabana->resultado_final_v2 / 100 }} </td>
            
            </tr>
        @endforeach
    </tbody>
</table>