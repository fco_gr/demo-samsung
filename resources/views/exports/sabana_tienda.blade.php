<?php
    function filtraRespuesta($valor){
        $valorFinal = $valor;
        if ($valorFinal == 99){
            $valorFinal = '';
        }
        else if ($valorFinal == 2){
            $valorFinal = 0;
        }

        return $valorFinal;
    }

?>


<table>
    <thead>
        <tr>
            <th style="background: #00ccff">COD_TAREA</th>
            <th style="background: #00ccff">SHOPER</th>
            <th style="background: #00ccff">AÑO</th>
            <th style="background: #00ccff">MES</th>
            <th style="background: #00ccff">FILE</th>
            <th style="background: #00ccff">VISITA</th>
            <th style="background: #00ccff">PLATAFORMA</th>
            <th style="background: #00ccff">IMAGEN</th>
            <th style="background: #00ccff">DIRECCION</th>
            <th style="background: #00ccff">COMUNA</th>
            <th style="background: #00ccff">REGION</th>
            <th style="background: #00ccff">JEFE DE ZONA</th>
            <th style="background: #00ccff">SUBGERENTE</th>
            <th style="background: #00ccff">NOMBRE COLABORADOR</th>
            <th style="background: #00ccff">FECHA</th>
            <th style="background: #00ccff">TRAMO DE HORA</th>


            <th style="background: #417FFC">NOTA2 Califique la actitud de amabilidad del atendedor(de 1 a 7)</th>
            <th style="background: #417FFC">P2_8 El Cajero se encontraba con su mascarilla, tapando boca y nariz</th>
            <th style="background: #417FFC">P2_8_1 Razón No cumplimiento P2_8_1</th>
            <th style="background: #417FFC">P2_19 ¿Hay mesas y sillas disponibles para clientes afuera de la tienda?</th>

            <th style="background: #99cc00">RESULTADO_FINAL</th>
            <th style="background: #99cc00">PUNTAJE_OBTENIDO</th>
            <th style="background: #99cc00">PUNTAJE_TOTAL</th>

            <th style="background: #008000">PUNTAJE BIENVENIDA TOTAL</th>
            <th style="background: #008000">P2_2 ¿El cajero de la tienda le dio un saludo cordial? </th>
            <th style="background: #008000">P2_9 ¿El colaborador en la posición de comida le dio un saludo cordial? </th>

            <th style="background: #ff9900">PUNTAJE RAPIDEZ TOTAL</th>
            <th style="background: #ff9900">P2_1 Si habían más de 3 personas en la fila, ¿se habilitó otra caja? </th>
            <th style="background: #ff9900">P2_4 ¿La transacción de pago en la tienda se completó de manera correcta, escaneo de productos riguroso y ágil? </th>
            <th style="background: #ff9900">P2_4_1 Razón No cumplimiento P2_4</th>
            <th style="background: #ff9900">P2_10 ¿Fue rápida la entrega de la comida/café? </th>

            <th style="background: #ffcc99">PUNTAJE CONFIANZA TOTAL</th>
            <th style="background: #ffcc99">P2_6 ¿El cajero que lo atendió en la tienda, llevaba su piocha de identificación con su nombre claramente legible?</th>
            <th style="background: #ffcc99">P2_6_1  Razón No cumplimiento P2_6</th>
            <th style="background: #ffcc99">P2_7 ¿El cajero que lo atendió en la tienda, llevaba su uniforme autorizado y limpio</th>
            <th style="background: #ffcc99">P2_7_1 Razón No cumplimiento P2_7</th>
            <th style="background: #ffcc99">P2_11 ¿El colaborador en la posición de comida cuenta con los implementos de higiene y seguridad definidos? </th>
            <th style="background: #ffcc99">P2_11_1 Razón No cumplimiento P2_11</th>

            <th style="background: #00ccff">PUNTAJE ACTITUD TOTAL</th>
            <th style="background: #00ccff">P2_3 ¿El cajero de la tienda ofreció producto complementario o promoción vigente?</th>
            <th style="background: #00ccff">P2_5 ¿El cajero de la tienda le dio las gracias y se despidió en forma amable? </th>
            <th style="background: #00ccff">P2_8 ¿El cajero se encontraba con su mascarilla, tapando boca y nariz?</th>
            <th style="background: #00ccff">P2_8_1 Razón No cumplimiento P2_8</th>
            <th style="background: #00ccff">P2_12 ¿El colaborador en la posición de comida le dio las gracias y se despidió de forma cordial? </th>
            <th style="background: #00ccff">P2_20 ¿El cajero de la tienda ofreció acumular CMR puntos?</th>
            
            <th style="background: #ccffff">PUNTAJE BAÑO TOTAL</th>
            <th style="background: #ccffff">P2_13 ¿Se encontraba el baño disponible para clientes?</th>
            <th style="background: #ccffff">P2_13_1 Razón No cumplimiento P2_13</th>
            <th style="background: #ccffff">P2_14 ¿El baño que utilizó estaba limpio e iluminado?</th>
            <th style="background: #ccffff">P2_14_1 Razón No cumplimiento P2_14</th>
            <th style="background: #ccffff">P2_15 ¿Los distintos elementos del baño que utilizó estaban en perfecto estado?</th>
            <th style="background: #ccffff">P2_15_1 Razón No cumplimiento P2_15</th>
            <th style="background: #ccffff">P2_16 ¿El baño que utilizó, estaba abastecido y funcionando en forma correcta?</th>
            <th style="background: #ccffff">P2_16_1 Razón No cumplimiento P2_16</th>
            <th style="background: #ccffff">P2_18 Nombre Boleta</th>

            <th style="background: #ffff99">PUNTAJE_OBTENIDO_V2</th>
            <th style="background: #ffff99">META</th>
            <th style="background: #ffff99">RESULTADO_FINAL_V2</th>
            


            
            


        </tr>
    </thead>
    <tbody>
        @foreach($sabanas as $sabana)
            <tr>
            <td>{{  $sabana->cod_tarea }}
            <td>{{ $sabana->cod_dooer }} </td>
            <td>{{  $sabana->agno }}
            <td>{{  $sabana->mes }}
            <td>{{  $sabana->id }}
            {{-- <td>{{  $sabana->descripcion }} --}}
            <td>{{ $sabana->VISITA }}</td>
            <td>{{  $sabana->plataforma }}
            <td>{{  $sabana->imagen_tienda }}
            <td>{{  $sabana->direccion }}
            <td>{{  $sabana->comuna }}
            <td>{{  $sabana->region }}
            <td>{{  $sabana->jefe_zona }}
            <td>{{  $sabana->subgerente_area }}
            <td>{{  $sabana->nom_colaborador }}
            <td>{{ $sabana->fecha_tramo }} </td>
            <td>{{ $sabana->horallegada_tramo }} </td>

            <td>{{  $sabana->NOTA2 }}
            <td>{{  filtraRespuesta($sabana->P2_8) }}
            <td>{{  $sabana->P2_8_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_19) }}


            <td>{{  $sabana->resultado_final / 100 }}
            <td>{{  $sabana->puntaje_obtenido }}
            <td>{{  $sabana->puntaje_total }}
            
                <td>{{  $sabana->puntaje_bienvenida_total / 100 }}
            <td>{{  filtraRespuesta($sabana->P2_2) }}
            <td>{{  filtraRespuesta($sabana->P2_9) }}
            
                <td>{{  $sabana->puntaje_rapidez_total / 100 }}
            <td>{{  filtraRespuesta($sabana->P2_1) }}
            <td>{{  filtraRespuesta($sabana->P2_4) }}
            <td>{{  $sabana->P2_4_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_10) }}
            
            <td>{{  $sabana->puntaje_confianza_total / 100 }}
            <td>{{  filtraRespuesta($sabana->P2_6) }}
            <td>{{  $sabana->P2_6_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_7) }}
            <td>{{  $sabana->P2_7_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_11) }}
            <td>{{  $sabana->P2_11_1_detalle }}
            
            <td>{{  $sabana->puntaje_actitud_total / 100 }}
            <td>{{  filtraRespuesta($sabana->P2_3) }}
            <td>{{  filtraRespuesta($sabana->P2_5) }}
            <td>{{  filtraRespuesta($sabana->P2_8) }}
            <td>{{  $sabana->P2_8_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_12) }}
            <td>{{  filtraRespuesta($sabana->P2_20) }}
            
            <td>{{  $sabana->puntaje_banos_total / 100 }}
            <td>{{  filtraRespuesta($sabana->P2_13) }}
            <td>{{  $sabana->P2_13_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_14) }}
            <td>{{  $sabana->P2_14_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_15) }}
            <td>{{  $sabana->P2_15_1_detalle }}
            <td>{{  filtraRespuesta($sabana->P2_16) }}
            <td>{{  $sabana->P2_16_1_detalle }}
            <td>{{  $sabana->P2_18 }}
            <td>{{  $sabana->resultado_final / 100 }}
            <td>{{  $sabana->meta_final }}
            <td>{{  $sabana->resultado_final_v2 / 100 }}
            </tr>
        @endforeach
    </tbody>
</table>