@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="far fa-file-alt"></i>
    Copia datos desde Doit
@endsection

@section('content')
    <sync></sync>
    
@endsection