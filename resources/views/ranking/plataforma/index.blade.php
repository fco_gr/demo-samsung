@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="fas fa-sort-amount-down"></i>
    <span>Ranking por Plataforma</span>
@endsection

@section('content')
<div class="row">
    <ranking_plataforma></ranking_plataforma>
</div>
    
@endsection