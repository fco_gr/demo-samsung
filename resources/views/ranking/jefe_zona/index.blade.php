@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="fas fa-sort-amount-down"></i>
    <span>Ranking por Jefe de Zona</span>
@endsection

@section('content')
<div class="row">
    <ranking_jefe_zona></ranking_jefe_zona>
</div>
    
@endsection