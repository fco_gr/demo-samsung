@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="fas fa-tachometer-alt"></i>
    Evaluaciones
@endsection

@section('content')
<div class="row">
    <evaluacion></evaluacion>
</div>
    
@endsection