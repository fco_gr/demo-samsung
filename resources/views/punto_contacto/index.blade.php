@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
<i class="fas fa-book"></i>
    Punto de Contacto
@endsection

@section('content')
<div class="row">
    <punto_contacto></punto_contacto>
</div>
    
@endsection