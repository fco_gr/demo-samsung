<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href="{{ asset ('img/favicon.png') }}" rel="icon" type="image/png">

        @yield('styles')

    </head>
    <body class="sb-nav-fixed">
        <div id="app">
            @include('common.header')
            <div id="layoutSidenav">
                @include('common.sidebar')
                <div id="layoutSidenav_content">
                    <main>
                        <div class="container-fluid px-4">
                            <h2 class="mt-4 text-primary" >@yield('titulo')</h2>
                            <ol class="breadcrumb mb-4">
                                <li class="breadcrumb-item active">@yield('breadcrumb')</li>
                            </ol>
                            @yield('content')
                        </div>
                    </main>
                    <vue-progress-bar></vue-progress-bar>
                    
                </div>
            </div>
            {{-- @include('common.footer') --}}
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        
        @auth
        <script>
            window.user = @json(auth()->user())
        </script>
        @endauth
        
        @yield('script')
        <script src="{{asset('js/scripts.js')}}"></script>

        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script> --}}
        {{-- <script src="{{asset('assets/demo/chart-area-demo.js')}}"></script>
        <script src="{{asset('assets/demo/chart-bar-demo.js')}}"></script>
        <script src="{{asset('assets/demo/chart-pie-demo.js')}}"></script> --}}


        {{-- <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script> --}}
        {{-- <script src="{{asset('js/datatables-simple-demo.js')}}"></script> --}}
    </body>
</html>