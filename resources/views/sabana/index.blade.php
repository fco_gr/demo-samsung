@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="far fa-file-alt"></i>
    Exporta datos Sabana
@endsection

@section('content')
    
    <table>
        {{-- <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead> --}}
        <tbody>
        @foreach($medicions as $medicion)
            <tr>
                <td>
                    <div class="card" style="width: 15rem;">
                            <div class="card-body text-center">
                                <i class="fas fa-database fa-4x black"></i>
                            </div>
                            <div class="card-body">
                                
                                <p class="card-text text-center">
                                    <a href="{{ route('sabana.index.recupera', [$medicion->id, 1]) }}" class="link-secondary">
                                        <i class="fas fa-download"></i>
                                        Mobile {{ $medicion->descripcion_corto }}
                                    </a>
                                </p>

                                @if ($medicion->id == 4)
                                    <p class="card-text text-center">
                                        <a href="{{ route('sabana.index.recupera', [$medicion->id, 11]) }}" class="link-secondary">
                                            <i class="fas fa-download"></i>
                                            Trimestre 
                                    </p>
                                @endif

                            </div>
                      </div>
                </td>

                <td>
                    <div class="card" style="width: 15rem;">
                            <div class="card-body text-center">
                                <i class="fas fa-database fa-4x black"></i>
                            </div>
                            <div class="card-body">
                                
                                <p class="card-text text-center">
                                    <a href="{{ route('sabana.index.recupera', [$medicion->id, 2]) }}" class="link-secondary">
                                        <i class="fas fa-download"></i>
                                        TV y AV {{ $medicion->descripcion_corto }}
                                    </a>
                                </p>

                                @if ($medicion->id == 4)
                                    <p class="card-text text-center">
                                        <a href="{{ route('sabana.index.recupera', [$medicion->id, 22]) }}" class="link-secondary">
                                            <i class="fas fa-download"></i>
                                            Trimestre 
                                        </a>
                                    </p>
                                @endif

                            </div>
                    </div>
                </td>

                <td>
                    <div class="card" style="width: 15rem;">
                            <div class="card-body text-center">
                                <i class="fas fa-database fa-4x black"></i>
                            </div>
                            <div class="card-body">
                                <p class="card-text text-center">
                                    <a href="{{ route('sabana.index.recupera', [$medicion->id, 3]) }}" class="link-secondary">
                                        <i class="fas fa-download"></i>
                                        Linea Blanca {{ $medicion->descripcion_corto }}
                                    </a>
                                </p>
                                @if ($medicion->id == 4)
                                    <p class="card-text text-center">
                                        <a href="{{ route('sabana.index.recupera', [$medicion->id, 33]) }}" class="link-secondary">
                                            <i class="fas fa-download"></i>
                                            Trimestre 
                                        </a>
                                    </p>
                                @endif
                            </div>
                      </div>
                </td>

                
                    
                    
                
            </tr>
        @endforeach
        </tbody>
    </table>    


@endsection