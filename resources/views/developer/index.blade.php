@extends('layouts.admin')

@section('content')
<div class="row">

    <passport-clients></passport-clients>
    <passport-authorized-clients></passport-authorized-clients>
    <passport-personal-access-tokens></passport-personal-access-tokens>
    
</div>
@endsection