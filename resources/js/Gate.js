export default class Gate{

    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type === 'admin';
    }

    isUser(){
        return this.user.type === 'user';
    }
    
    isAdminOrUser(){
        if(this.user.type === 'user' || this.user.type === 'admin'){
            return true;
        }
    }

    isGerente(){
        return this.user.type === 'gerente';
    }

    isSubGerente(){
        return this.user.type === 'sg_gerente';
    }

    isJefeZona(){
        return this.user.type === 'j_zona';
    }

    isJefeTienda(){
        return this.user.type === 'j_tienda';
    }

    isJefeOperacion(){
        return this.user.type === 'j_operacion';
    }

    isFile(){
        return this.user.type === 'file';
    }

    isBiblioteca(){
        if(this.user.type === 'admin' || this.user.type === 'gerente'){
            return true;
        }
    }

}
