/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import moment from 'moment';

// import { Form, HasError, AlertError } from 'vform';
// window.Form = Form;

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '14px'
  })


import Swal from 'sweetalert2'
import Vue from 'vue';
window.swal = Swal;

import Sparkline from 'vue-sparklines'
Vue.use(Sparkline)

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

window.toast = Toast;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('pagination', require('laravel-vue-pagination'));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('not-found', require('./components/NotFound.vue').default);
Vue.component('users', require('./components/Users.vue').default);

Vue.component('dashboard', require('./components/Dashboard/Dashboard.vue').default);
Vue.component('biblioteca', require('./components/Biblioteca/Biblioteca.vue').default);
Vue.component('punto_contacto', require('./components/PuntoContacto/PuntoContacto.vue').default);
Vue.component('evaluacion', require('./components/Evaluacion/Evaluacion.vue').default);

Vue.component('ranking_subgerente', require('./components/Ranking/RankingSubgerente.vue').default);
Vue.component('ranking_jefe_zona', require('./components/Ranking/RankingJefeZona.vue').default);
Vue.component('ranking_plataforma', require('./components/Ranking/RankingPlataforma.vue').default);
Vue.component('ranking_file', require('./components/Ranking/RankingFile.vue').default);

Vue.component('sync', require('./components/Doit/Sync.vue').default);
Vue.component('pivot', require('./components/Pivot/Pivot.vue').default);


Vue.component('passport-clients', require('./components/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

// Filter


Vue.filter('myDate',function(created){
  return moment(created).format('MMMM Do YYYY');
});

Vue.filter('myDateShort', function(fecha){
  return moment(fecha).format('DD-MM-YYYY');
});

Vue.filter('myDateLong', function(fecha){
  return moment(fecha).format('DD-MM-YYYY h:mm:ss');
});


Vue.filter('yesno', value => (value ? '<i class="fas fa-check green"></i>' : '<i class="fas fa-times red"></i>'));

// end Filter

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
