<?php

namespace App\Http\Controllers;

use App\Imports\EdsImport;
use App\Imports\EdsImportCombustible;
use App\Imports\EdsImportLavado;
use App\Imports\EdsImportTienda;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EdsImportController extends Controller
{
    public function show(){
        return view('eds.import');
    }

    public function store(Request $request){
        
        $request->validate([
            'pauta' => 'required',
            'file' => 'required'
        ]);

        $file = $request->file('file')->store('import');

        // dd($request->pauta);

        // Excel::import(new EdsImportCombustible, $request->file);

        if ($request->pauta == '1'){
            Excel::import(new EdsImportCombustible, $request->file);
        }
        else if ($request->pauta == '2'){
            Excel::import(new EdsImportTienda, $request->file);
        }
        else if ($request->pauta == '3'){
            Excel::import(new EdsImportLavado, $request->file);
        }



        // $file = $request->file('file');
        // dd($file);

        //$data = Excel::toArray(new EdsImport(), $request->file);

        //dd($data);

        //Excel::import(new EdsImport, $request->file);

        // $import = new EdsImport;
        // $import->import($file);

        // if ($import->failures()->isNotEmpty()) {
        //     return back()->withFailures($import->failures());
        // }


        return back()->withStatus('Import in queue, we will send notification after import finished.');
    }

}
