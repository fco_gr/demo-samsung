<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class BibliotecaController extends BaseController
{
    public function getDatos(Request $request){

        // dd(!Gate::allows('isSabana'));
        // isAdmin', 'isGerente
        if (!Gate::allows('isAdmin') && !Gate::allows('isGerente')) {
            return $this->unauthorizedResponse();
        }
        
        
        $where = aplicaFiltros($request);
        $sql = "select eds_id, medicions.descripcion, FOTO_BOL1, FOTO_BOL2, FOTO_BOL3, FOTO_BANO, FOTO_BANO2, FOTO_BANO2 ";
        $sql .= "from datos_consolidados, estudios, medicions, eds  ";
        $sql .= "where datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and datos_consolidados.eds_id = eds.id   $where ";

        $datos = DB::select($sql);
        return $this->sendResponse($datos,  'Datos biblioteca');
    }

    
}
