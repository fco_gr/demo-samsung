<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class RankingJefeZonaController extends BaseController
{
    public function getRanking(Request $request){

        if (Gate::allows('isFile')) {
            return $this->unauthorizedResponse();
        }

        $where = aplicaFiltros($request);
        $whereFiltroPerfil = aplicaFiltrosPerfil();

        $typePerfil = Auth::user()->type;
        if ($typePerfil == 'j_zona'){
            $whereFiltroPerfil = aplicaFiltrosPerfilBySubGerente();
        }
        // dd($whereFiltroPerfil);

        $decimales = 2;

        $sql = "select eds.jefe_de_zona_nombre as nombre,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total,  ";
        $sql .= "round((sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100, $decimales) as porc  ";
        $sql .= "from   ";
        $sql .= "datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, estudios, medicions  ";
        $sql .= "where   ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND   ";
        $sql .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0  and eds.jefe_de_zona_nombre is not null and eds.jefe_de_zona_nombre <> ''                 $where  $whereFiltroPerfil  ";
        $sql .= "group by eds.jefe_de_zona_nombre  ";
        $sql .= "order by porc desc; ";

        // dd($sql);

        $rankings = DB::select($sql);
        $labels=[];
        $valores=[];

        foreach ($rankings as $ranking){
            $labels[] = $ranking->nombre;
            $valores[] = $ranking->porc;

            if($ranking->porc < 80) $color = "#CB2F31";
            // amarillo
            elseif($ranking->porc >= 80 && $ranking->porc < 90) $color = "#D4CF3E";
            // Verde
            elseif($ranking->porc >= 90) $color = "#70C15C";

           
            //$color = 'rgba(3, 101, 192, 0.5)';
            $borde = 'rgb(3, 101, 192)';

            $colores[] = $color;
            // $bordes[] = $borde;
            $bordes[] = [];
        }

        return $this->sendResponse(
            [
                'datos' => $rankings,
                'valores' => $valores,
                'labels' => $labels,
                'colores' => $colores,
                'bordes' => $bordes
            ],  'Datos ranking Subgerente');
    }
}
