<?php

namespace App\Http\Controllers\API\V1;

use App\Eds;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VariableFiltroController extends BaseController
{
    public function listFiles()
    {
        $listadoFile = Eds::select('id')->orderBy('id')->distinct()->pluck('id');
        return $this->sendResponse($listadoFile, 'Lista file');
        // $tipoNecesidades = DatoConsolidado::select('TIPO_NECES_OP')->orderBy('TIPO_NECES_OP')->distinct()->get()->pluck('TIPO_NECES_OP');
        // return $this->sendResponse($tipoNecesidades, 'Lista necesidades');
    }
}
