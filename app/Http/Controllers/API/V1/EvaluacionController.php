<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\Console\Input\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class EvaluacionController extends BaseController
{

    private function recuperaRespuestas($respuestas, $campo, $valor){
        if ($valor != ''){
            $lfcr = chr(13).chr(13);
            $listaValores = explode(",", $valor);
            $texto = '';
            try {
                for ($i=0; $i<count($listaValores); $i++){
                    $texto .= $listaValores[$i] .' '. $respuestas[$campo][$listaValores[$i]];
                    if ($i != count($listaValores) - 1){
                        $texto .= $lfcr;
                    }
                }
            } catch (\Throwable $th) {
                // dd($respuestas, $campo, $valor);
            }
            
            return $texto;
        }
        return $valor;
    }

    public function show(Request $request){
        $codTarea = $request->cod_tarea;
        $pauta = $request->pauta;

        $wherePregunta = "";
        if ($codTarea >= 2108180000){
            $wherePregunta = " and pregunta <> 'P1_11' ";
        } 

        // $where = aplicaFiltros($request);


        $decimales = 1;

        $sqlGlobal = "select eds.id, datos_consolidados.cod_tarea, sum(datos_consolidados_detalles.puntaje) as sum_puntaje, sum(datos_consolidados_detalles.puntaje_total) as sum_puntaje_total,   ";
        $sqlGlobal .= "round((sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100, $decimales) as porc,  ";
        $sqlGlobal .= "sum(if (pilar_id = 1, datos_consolidados_detalles.puntaje, 0)) as pilar_1_puntaje,   ";
        $sqlGlobal .= "sum(if (pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) as pilar_1_puntaje_total,  ";
        $sqlGlobal .= "if (sum(if (pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) > 0,  ";
        $sqlGlobal .= "round(sum(if (pilar_id = 1, datos_consolidados_detalles.puntaje, 0)) / sum(if (pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) * 100, $decimales), 0 ";
        $sqlGlobal .= ") as pilar_1_porc, ";
        $sqlGlobal .= "sum(if (pilar_id = 2, datos_consolidados_detalles.puntaje, 0)) as pilar_2_puntaje,   ";
        $sqlGlobal .= "sum(if (pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) as pilar_2_puntaje_total,  ";
        $sqlGlobal .= "if (sum(if (pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) > 0,  ";
        $sqlGlobal .= "round(sum(if (pilar_id = 2, datos_consolidados_detalles.puntaje, 0)) / sum(if (pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) * 100, $decimales), 0 ";
        $sqlGlobal .= ") as pilar_2_porc, ";
        $sqlGlobal .= "sum(if (pilar_id = 3, datos_consolidados_detalles.puntaje, 0)) as pilar_3_puntaje,   ";
        $sqlGlobal .= "sum(if (pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) as pilar_3_puntaje_total,  ";
        $sqlGlobal .= "if (sum(if (pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) > 0,  ";
        $sqlGlobal .= "round(sum(if (pilar_id = 3, datos_consolidados_detalles.puntaje, 0)) / sum(if (pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) * 100, $decimales), 0 ";
        $sqlGlobal .= ") as pilar_3_porc, ";
        $sqlGlobal .= "sum(if (pilar_id = 4, datos_consolidados_detalles.puntaje, 0)) as pilar_4_puntaje,   ";
        $sqlGlobal .= "sum(if (pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) as pilar_4_puntaje_total,  ";
        $sqlGlobal .= "if (sum(if (pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) > 0,  ";
        $sqlGlobal .= "round(sum(if (pilar_id = 4, datos_consolidados_detalles.puntaje, 0)) / sum(if (pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) * 100, $decimales), 0 ";
        $sqlGlobal .= ") as pilar_4_porc, ";
        $sqlGlobal .= "sum(if (pilar_id = 5, datos_consolidados_detalles.puntaje, 0)) as pilar_5_puntaje,   ";
        $sqlGlobal .= "sum(if (pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) as pilar_5_puntaje_total,  ";
        $sqlGlobal .= "if (sum(if (pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) > 0,  ";
        $sqlGlobal .= "round(sum(if (pilar_id = 5, datos_consolidados_detalles.puntaje, 0)) / sum(if (pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) * 100, $decimales), 0 ";
        $sqlGlobal .= ") as pilar_5_porc ";
        $sqlGlobal .= "from datos_consolidados_detalles, datos_consolidados, eds, preguntas, estudios, medicions  ";
        $sqlGlobal .= "where datos_consolidados_detalles.cod_tarea = datos_consolidados.cod_tarea AND   ";
        $sqlGlobal .= "datos_consolidados.eds_id = eds.id and   ";
        $sqlGlobal .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and   ";
        $sqlGlobal .= "datos_consolidados_detalles.pregunta_id = preguntas.id and status in (5, 6) and preguntas.pauta_id = $pauta and datos_consolidados.cod_tarea = $codTarea ";
        $sqlGlobal .= "group by eds.id, datos_consolidados.cod_tarea  ";
        $sqlGlobal .= "order by eds.id";

        $globales = collect(DB::select($sqlGlobal))->first();

        $sqlSucursal = "select datos_consolidados.*, eds.razon_social, eds.direccion, eds.comuna, eds.region, eds.jefe_de_zona_nombre as jefe_zona, eds.subgerente_area_nombre as subgerente_area, eds.plataforma, eds.id, DATE_FORMAT(FEC_EVAL, '%d-%m-%Y') as fecha_eval, eds.jefe_de_tienda_nombre as j_zona_tienda, eds.jefe_lavado, eds.jefe_operacion_tienda_nombre as s_zona_tienda  ";
        $sqlSucursal .= "from datos_consolidados, eds ";
        $sqlSucursal .= "where datos_consolidados.eds_id = eds.id and datos_consolidados.cod_tarea = $codTarea ";

        // Respuestas
        $respuestas = [];
        $respuestasDBs = DB::select("select * from respuestas order by id");
        foreach ($respuestasDBs as $respuestasDB){
            $respuestas[$respuestasDB->campo][$respuestasDB->valor] = $respuestasDB->descripcion;
        }

        $sucursal = collect(DB::select($sqlSucursal))->first();
       
        if ($pauta == 1){
            if ($sucursal->P1_2_1 != null){
                $sucursal->P1_2_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_2_1', $sucursal->P1_2_1);
            } else {
                $sucursal->P1_2_1_detalle = '';
            }
            if ($sucursal->P1_3_1 != null){
                $sucursal->P1_3_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_3_1', $sucursal->P1_3_1);
            } else {
                $sucursal->P1_3_1_detalle = '';
            }
            if ($sucursal->P1_1_1 != null){
                $sucursal->P1_1_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_1_1', $sucursal->P1_1_1);
            } else {
                $sucursal->P1_1_1_detalle = '';
            }
            if ($sucursal->P1_4_1 != null){
                $sucursal->P1_4_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_4_1', $sucursal->P1_4_1);
            } else {
                $sucursal->P1_4_1_detalle = '';
            }
            if ($sucursal->P1_13_1 != null){
                $sucursal->P1_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_13_1', $sucursal->P1_13_1);
            } else {
                $sucursal->P1_13_1_detalle = '';
            }
            if ($sucursal->P1_15_1 != null){
                $sucursal->P1_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_15_1', $sucursal->P1_15_1);
            } else {
                $sucursal->P1_15_1_detalle = '';
            }
            if ($sucursal->P1_16_1 != null){
                $sucursal->P1_16_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_16_1', $sucursal->P1_16_1);
            } else {
                $sucursal->P1_16_1_detalle = '';
            }

            if ($sucursal->P1_21_1 != null){
                $sucursal->P1_21_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_21_1', $sucursal->P1_21_1);
            } else {
                $sucursal->P1_21_1_detalle = '';
            }

            if ($sucursal->P1_6_1 != null){
                $sucursal->P1_6_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_6_1', $sucursal->P1_6_1);
            } else {
                $sucursal->P1_6_1_detalle = '';
            }

            if ($sucursal->P1_9_1 != null){
                $sucursal->P1_9_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_9_1', $sucursal->P1_9_1);
            } else {
                $sucursal->P1_9_1_detalle = '';
            }


            
            
            
            
            if ($sucursal->P1_8 != null){
                if ($sucursal->P1_8 == 1){
                    $sucursal->P1_8_1 = 1;
                    $sucursal->P1_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_8_1', $sucursal->P1_8_1);
                }
                else {
                    $sucursal->P1_8_1 = 2;
                    $sucursal->P1_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_8_2', $sucursal->P1_8_2);
                }
            } else {
                $sucursal->P1_8_1_detalle = '';
            }
            if ($sucursal->P1_11_1 != null){
                $sucursal->P1_11_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_11_1', $sucursal->P1_11_1);
            } else {
                $sucursal->P1_11_1_detalle = '';
            }
            if ($sucursal->P1_17_1 != null){
                $sucursal->P1_17_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_17_1', $sucursal->P1_17_1);
            } else {
                $sucursal->P1_17_1_detalle = '';
            }

            if ($sucursal->P1_5 == 2){
                $sucursal->P1_5_1 = 2;
                $sucursal->P1_5_1_detalle = $sucursal->P1_5_TXT;
            } else {
                $sucursal->P1_5_1_detalle = '';
            }

            if ($sucursal->P1_7 == 2){
                $sucursal->P1_7_1 = 2;
                $sucursal->P1_7_1_detalle = $sucursal->P1_7_TXT;
            } else {
                $sucursal->P1_7_1_detalle = '';
            }


            if ($sucursal->P1_10 != null){
                if ($sucursal->P1_10 == 1){
                    $sucursal->P1_10_1 = 1;
                    $sucursal->P1_10_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_10_1', $sucursal->P1_10_1);
                }
                else if ($sucursal->P1_10 == 2) {
                    $sucursal->P1_10_1 = 2;
                    $sucursal->P1_10_1_detalle = $sucursal->P1_10_TXT;
                }
            } else {
                $sucursal->P1_10_1_detalle = '';
            }

            if ($sucursal->P1_19 == 2){
                $sucursal->P1_19_1 = 2;
                $sucursal->P1_19_1_detalle = $sucursal->P1_19_TXT;
            } else {
                $sucursal->P1_19_1_detalle = '';
            }

            if ($sucursal->P1_12 == 2){
                $sucursal->P1_12_1 = 2;
                $sucursal->P1_12_1_detalle = $sucursal->P1_12_TXT;
            } else {
                $sucursal->P1_12_1_detalle = '';
            }

            if ($sucursal->P1_14 == 2){
                $sucursal->P1_14_1 = 2;
                $sucursal->P1_14_1_detalle = $sucursal->P1_14_TXT;
            } else {
                $sucursal->P1_14_1_detalle = '';
            }

        }
        else if ($pauta == 2){
            if ($sucursal->P2_4_1 != null){
                $sucursal->P2_4_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_4_1', $sucursal->P2_4_1);
            } else {
                $sucursal->P2_4_1_detalle = '';
            }

            if ($sucursal->P2_6_1 != null){
                $sucursal->P2_6_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_6_1', $sucursal->P2_6_1);
            } else {
                $sucursal->P2_6_1_detalle = '';
            }

            if ($sucursal->P2_7_1 != null){
                $sucursal->P2_7_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_7_1', $sucursal->P2_7_1);
            } else {
                $sucursal->P2_7_1_detalle = '';
            }

            if ($sucursal->P2_11_1 != null){
                $sucursal->P2_11_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_11_1', $sucursal->P2_11_1);
            } else {
                $sucursal->P2_11_1_detalle = '';
            }

            if ($sucursal->P2_8_1 != null){
                $sucursal->P2_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_8_1', $sucursal->P2_8_1);
            } else {
                $sucursal->P2_8_1_detalle = '';
            }

            if ($sucursal->P2_13_1 != null){
                $sucursal->P2_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_13_1', $sucursal->P2_13_1);
            } else {
                $sucursal->P2_13_1_detalle = '';
            }

            if ($sucursal->P2_14_1 != null){
                $sucursal->P2_14_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_14_1', $sucursal->P2_14_1);
            } else {
                $sucursal->P2_14_1_detalle = '';
            }

            if ($sucursal->P2_15_1 != null){
                $sucursal->P2_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_15_1', $sucursal->P2_15_1);
            } else {
                $sucursal->P2_15_1_detalle = '';
            }

            if ($sucursal->P2_16_1 != null){
                $sucursal->P2_16_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_16_1', $sucursal->P2_16_1);
            } else {
                $sucursal->P2_16_1_detalle = '';
            }


            if ($sucursal->P2_1 == 2){
                $sucursal->P2_1_1 = 2;
                $sucursal->P2_1_1_detalle = $sucursal->P2_1_TXT;
            } else {
                $sucursal->P2_1_1_detalle = '';
            }
            if ($sucursal->P2_2 == 2){
                $sucursal->P2_2_1 = 2;
                $sucursal->P2_2_1_detalle = $sucursal->P2_2_TXT;
            } else {
                $sucursal->P2_2_1_detalle = '';
            }
            if ($sucursal->P2_3 == 2){
                $sucursal->P2_3_1 = 2;
                $sucursal->P2_3_1_detalle = $sucursal->P2_3_TXT;
            } else {
                $sucursal->P2_3_1_detalle = '';
            }
            if ($sucursal->P2_20 == 2){
                $sucursal->P2_20_1 = 2;
                $sucursal->P2_20_1_detalle = $sucursal->P2_20_TXT;
            } else {
                $sucursal->P2_20_1_detalle = '';
            }
            if ($sucursal->P2_5 == 2){
                $sucursal->P2_5_1 = 2;
                $sucursal->P2_5_1_detalle = $sucursal->P2_5_TXT;
            } else {
                $sucursal->P2_5_1_detalle = '';
            }
            if ($sucursal->P2_9 == 2){
                $sucursal->P2_9_1 = 2;
                $sucursal->P2_9_1_detalle = $sucursal->P2_9_TXT;
            } else {
                $sucursal->P2_9_1_detalle = '';
            }
            if ($sucursal->P2_10 == 2){
                $sucursal->P2_10_1 = 2;
                $sucursal->P2_10_1_detalle = $sucursal->P2_10_TXT;
            } else {
                $sucursal->P2_10_1_detalle = '';
            }
            if ($sucursal->P2_12 == 2){
                $sucursal->P2_12_1 = 2;
                $sucursal->P2_12_1_detalle = $sucursal->P2_12_TXT;
            } else {
                $sucursal->P2_12_1_detalle = '';
            }

        }
        else if ($pauta == 3){
            // dd($sucursal->P3_2);

            if ($sucursal->P3_1 == 2){
                $sucursal->P3_1_1 = 2;
                $sucursal->P3_1_1_detalle = $sucursal->P1_3_TXT;
            } else {
                $sucursal->P3_1_1_detalle = '';
            }

            
            if ($sucursal->P3_5_1 != null){
                $sucursal->P3_5_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_5_1', $sucursal->P3_5_1);
            } else {
                $sucursal->P3_5_1_detalle = '';
            }

            if ($sucursal->P3_13_1 != null){
                $sucursal->P3_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_13_1', $sucursal->P3_13_1);
            } else {
                $sucursal->P3_13_1_detalle = '';
            }

            if ($sucursal->P3_14_1 != null){
                $sucursal->P3_14_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_14_1', $sucursal->P3_14_1);
            } else {
                $sucursal->P3_14_1_detalle = '';
            }

            if ($sucursal->P3_15_1 != null){
                $sucursal->P3_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_15_1', $sucursal->P3_15_1);
            } else {
                $sucursal->P3_15_1_detalle = '';
            }

            if ($sucursal->P3_4 != null){
                if ($sucursal->P3_4 == 1){
                    $sucursal->P3_4_1 = 1;
                    $sucursal->P3_4_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_4_1', $sucursal->P3_4_1);
                }
                else if ($sucursal->P3_4 == 2) {
                    $sucursal->P3_4_1 = 2;
                    $sucursal->P3_4_1_detalle = $sucursal->P3_4_TXT;
                }
            } else {
                $sucursal->P3_4_1_detalle = '';
            }

            // if ($sucursal->P3_1 == 2){
            //     $sucursal->P3_1_1 = 2;
            //     $sucursal->P3_1_1_detalle = $sucursal->P1_3_TXT;
            // } else {
            //     $sucursal->P3_1_1_detalle = '';
            // }

            if ($sucursal->P3_2 == 2){
                $sucursal->P3_2_1 = 2;
                $sucursal->P3_2_1_detalle = $sucursal->P3_2_TXT;
            } else {
                $sucursal->P3_2_1_detalle = '';
            }
            if ($sucursal->P3_3 == 2){
                $sucursal->P3_3_1 = 2;
                $sucursal->P3_3_1_detalle = $sucursal->P3_3_TXT;
            } else {
                $sucursal->P3_3_1_detalle = '';
            }
            if ($sucursal->P3_5 == 2){
                $sucursal->P3_5_1 = 2;
                $sucursal->P3_5_1_detalle = $sucursal->P3_5_1_TXT;
            } else {
                $sucursal->P3_5_1_detalle = '';
            }
            if ($sucursal->P3_6 == 2){
                $sucursal->P3_6_1 = 2;
                $sucursal->P3_6_1_detalle = $sucursal->P3_6_TXT;
            } else {
                $sucursal->P3_6_1_detalle = '';
            }
            if ($sucursal->P3_7 == 2){
                $sucursal->P3_7_1 = 2;
                $sucursal->P3_7_1_detalle = $sucursal->P3_7_TXT;
            } else {
                $sucursal->P3_7_1_detalle = '';
            }


            if ($sucursal->P3_8 == 2){
                $sucursal->P3_8_1 = 2;
                $sucursal->P3_8_1_detalle = $sucursal->P3_8_TXT;
            } else {
                $sucursal->P3_8_1_detalle = '';
            }
            if ($sucursal->P3_9 == 2){
                $sucursal->P3_9_1 = 2;
                $sucursal->P3_9_1_detalle = $sucursal->P3_9_TXT;
            } else {
                $sucursal->P3_9_1_detalle = '';
            }
            if ($sucursal->P3_10 == 2){
                $sucursal->P3_10_1 = 2;
                $sucursal->P3_10_1_detalle = $sucursal->P3_10_TXT;
            } else {
                $sucursal->P3_10_1_detalle = '';
            }
            if ($sucursal->P3_11 == 2){
                $sucursal->P3_11_1 = 2;
                $sucursal->P3_11_1_detalle = $sucursal->P3_11_TXT;
            } else {
                $sucursal->P3_11_1_detalle = '';
            }
            if ($sucursal->P3_12 == 2){
                $sucursal->P3_12_1 = 2;
                $sucursal->P3_12_1_detalle = $sucursal->P3_12_TXT;
            } else {
                $sucursal->P3_12_1_detalle = '';
            }

            if ($sucursal->P3_17 == 2){
                $sucursal->P3_17_1 = 2;
                $sucursal->P3_17_1_detalle = $sucursal->P3_17_TXT;
            } else {
                $sucursal->P3_17_1_detalle = '';
            }



        }

        $sqlPilar = "select pilar_id, datos_consolidados_detalles.*, preguntas.descripcion  ";
        $sqlPilar .= "from datos_consolidados_detalles, datos_consolidados, preguntas ";
        $sqlPilar .= "where datos_consolidados_detalles.cod_tarea = datos_consolidados.cod_tarea AND   ";
        $sqlPilar .= "datos_consolidados_detalles.pregunta_id = preguntas.id and pauta_id=$pauta  and  ";
        $sqlPilar .= "datos_consolidados_detalles.cod_tarea=$codTarea   $wherePregunta ";
        $sqlPilar .= "order by pilar_id, pregunta_id";

        // dd($sqlPilar);

        $pilares = DB::select($sqlPilar);
        $datoPilares = [];

        foreach ($pilares as $pilare){
            $datoPilares['pilar_' . $pilare->pilar_id][$pilare->pregunta] = $pilare;
        }

        return $this->sendResponse([
            'globales' => $globales,
            'sucursal' => $sucursal,
            'pilares' => $datoPilares
        ],  'Dato evaluacion');


    }

    public function getDatos(Request $request){

        $where = aplicaFiltros($request);
        $whereFiltroPerfil = aplicaFiltrosPerfil();

        $mes_ini = mesInicial();

        // $file = $request->filtro ? $request->filtro["file"] : [];

        $whereFile = "";
        // if ($file != ''){
        //     $whereFile = " and (eds.id = '$file' or eds.direccion like '%$file%') ";
        // }
        // dd($file);

        $decimales = 1;

        $sql = "select datos_consolidados.cod_tarea, eds.id, eds.direccion, DATE_FORMAT(FEC_EVAL, '%d-%m-%Y') as FEC_EVAL,  ";
        $sql .= "sum(if (preguntas.pauta_id = 1, datos_consolidados_detalles.puntaje, 0)) as puntaje_isla,   ";
        $sql .= "sum(if (preguntas.pauta_id = 1, datos_consolidados_detalles.puntaje_total , 0)) as puntaje_total_isla,   ";
        $sql .= "count(if (preguntas.pauta_id = 1, 1 , 0)) as cuenta_total_isla,   ";
        $sql .= "if (sum(if (preguntas.pauta_id = 1, datos_consolidados_detalles.puntaje_total, 0)) > 0, ";
        $sql .= "round(sum(if (preguntas.pauta_id = 1, datos_consolidados_detalles.puntaje, 0)) / sum(if (preguntas.pauta_id = 1, datos_consolidados_detalles.puntaje_total , 0)) * 100, $decimales), 0) as porc_isla,  ";
        $sql .= "sum(if (preguntas.pauta_id = 2, datos_consolidados_detalles.puntaje, 0)) as puntaje_tienda,   ";
        $sql .= "sum(if (preguntas.pauta_id = 2, datos_consolidados_detalles.puntaje_total , 0)) as puntaje_total_tienda,   ";
        $sql .= "count(if (preguntas.pauta_id = 2, 1 , 0)) as cuenta_total_tienda,   ";
        $sql .= "if (sum(if (preguntas.pauta_id = 2, datos_consolidados_detalles.puntaje_total , 0)) > 0,  ";
        $sql .= "round(sum(if (preguntas.pauta_id = 2, datos_consolidados_detalles.puntaje, 0)) / sum(if (preguntas.pauta_id = 2, datos_consolidados_detalles.puntaje_total , 0)) * 100, $decimales), 0) as porc_tienda,  ";
        $sql .= "sum(if (preguntas.pauta_id = 3, datos_consolidados_detalles.puntaje, 0)) as puntaje_lavado,   ";
        $sql .= "sum(if (preguntas.pauta_id = 3, datos_consolidados_detalles.puntaje_total , 0)) as puntaje_total_lavado,   ";
        $sql .= "count(if (preguntas.pauta_id = 3, 1 , 0)) as cuenta_total_lavado,   ";
        $sql .= "if (sum(if (preguntas.pauta_id = 3, datos_consolidados_detalles.puntaje_total , 0)) > 0,  ";
        $sql .= "round(sum(if (preguntas.pauta_id = 3, datos_consolidados_detalles.puntaje, 0)) / sum(if (preguntas.pauta_id = 3, datos_consolidados_detalles.puntaje_total , 0)) * 100, $decimales), 0) as porc_lavado,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, round((sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100, $decimales) as porc  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, estudios, medicions  ";
        $sql .= "where   ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND   ";
        $sql .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and   ";
        $sql .= "preguntas.puntaje > 0                         $where  $whereFiltroPerfil ";
        $sql .= "group by datos_consolidados.cod_tarea, eds.id, eds.direccion, FEC_EVAL  ";
        $sql .= "order by eds.id";

        // dd($sql);
        $evaluaciones = DB::select($sql);

        return $this->sendResponse(
            [
                'evaluaciones' => $evaluaciones,
                'mes_inicial' => $mes_ini
            ],  
        'Datos evaluaciones');
    }
}
