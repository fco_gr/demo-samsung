<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class RankingFileController extends BaseController
{
    public function getRanking(Request $request){

        if (Gate::allows('isFile')) {
            return $this->unauthorizedResponse();
        }

        $where = aplicaFiltros($request);
        $whereFiltroPerfil = aplicaFiltrosPerfil();

        // dd($whereFiltroPerfil);

        $decimales = 2;

        $sql = "select eds.id, eds.direccion,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total,  ";
        $sql .= "round((sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100, $decimales) as porc  ";
        $sql .= "from   ";
        $sql .= "datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, estudios, medicions  ";
        $sql .= "where   ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND   ";
        $sql .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0                       $where   $whereFiltroPerfil";
        $sql .= "group by eds.id, eds.direccion  ";
        $sql .= "order by porc desc, id;";

        // dd($sql);

        $rankings = DB::select($sql);
        $labels=[];
        $valores=[];
        
        return $this->sendResponse($rankings, 'Datos ranking Subgerente');

    }
}
