<?php

namespace App\Http\Controllers\API\V1;

use App\DatosConsolidado;
use Illuminate\Http\Request;
use App\DatosConsolidadosDetalle;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DatoConsolidadoController extends BaseController
{

    function TotalesCero(&$totalesEvaluacion, $pauta_id){
        for ($i=0; $i < count($totalesEvaluacion[$pauta_id]); $i++){
            $totalesEvaluacion[$pauta_id][$i] = 0;
        }
        return true; 
    }
    
    function TotalesIsla(&$totalesEvaluacion, $rs){
        $sqlPreguntas = "select * from preguntas where puntaje > 0 and pauta_id=1;";

        $preguntas = DB::select($sqlPreguntas);

        foreach ($preguntas as $pregunta){
            
            $preguntaCampo = $pregunta->campo;

            // dd($preguntaCampo, $rs->$preguntaCampo);
            $puntaje = 0;
            $puntaje_total = 0;
            if ($rs->$preguntaCampo == 2) {
                if ($preguntaCampo == 'P1_13' && ($rs->P1_13_1 == '9' || $rs->P1_13_1 == '10' || $rs->P1_13_1 == '9,10')){
                    $puntaje = 0;
                    $puntaje_total = 0;
                }
                else {
                    $puntaje = 0;
                    $puntaje_total = $pregunta->puntaje;
                }
            }
            else if ($rs->$preguntaCampo == 1) {
                $puntaje = $pregunta->puntaje;
                $puntaje_total = $pregunta->puntaje;
            }

            if ($puntaje_total >= 0){
                $respuesta = $rs->$preguntaCampo != "" ? $rs->$preguntaCampo : null;
                $datosConsolidadoDetalle = DatosConsolidadosDetalle::create([
                    'cod_estudio' => $rs->cod_estudio,
                    'cod_tarea' => $rs->cod_tarea,
                    'pregunta_id' => $pregunta->id,
                    'pregunta' => $preguntaCampo,
                    'respuesta' => $respuesta,
                    'puntaje' => $puntaje,
                    'puntaje_total' => $puntaje_total
                ]);
                // dd($datosConsolidadoDetalle);
            }

        }


        
    }

    function TotalesTienda(&$totalesEvaluacion, $rs){
        $sqlPreguntas = "select * from preguntas where puntaje > 0 and pauta_id=2;";

        $preguntas = DB::select($sqlPreguntas);

        foreach ($preguntas as $pregunta){
            $preguntaCampo = $pregunta->campo;
            $puntaje = 0;
            $puntaje_total = 0;
            if ($rs->$preguntaCampo == 2) {
                //if ($preguntaCampo == 'P1_13' && ($rs->P1_13_1 == '9' || $rs->P1_13_1 == '10' || $rs->P1_13_1 == '9,10')){
                if ($preguntaCampo == 'P2_4' && (in_array ('1', explode(',', $rs->P2_4_1)) || in_array ('3', explode(',', $rs->P2_4_1)) || in_array ('8', explode(',', $rs->P2_4_1)))){
                    $puntaje = 0;
                    $puntaje_total = 0;
                }
                else {
                    $puntaje = 0;
                    $puntaje_total = $pregunta->puntaje;
                }
            }
            else if ($rs->$preguntaCampo == 1) {
                $puntaje = $pregunta->puntaje;
                $puntaje_total = $pregunta->puntaje;
            }

            if ($puntaje_total >= 0){
                $respuesta = $rs->$preguntaCampo != "" ? $rs->$preguntaCampo : null;
                $datosConsolidadoDetalle = DatosConsolidadosDetalle::create([
                    'cod_estudio' => $rs->cod_estudio,
                    'cod_tarea' => $rs->cod_tarea,
                    'pregunta_id' => $pregunta->id,
                    'pregunta' => $preguntaCampo,
                    'respuesta' => $respuesta,
                    'puntaje' => $puntaje,
                    'puntaje_total' => $puntaje_total
                ]);
                // dd($datosConsolidadoDetalle);
            }
        }
    }

    function TotalesLavado(&$totalesEvaluacion, $rs){
        $sqlPreguntas = "select * from preguntas where puntaje > 0 and pauta_id=3;";

        $preguntas = DB::select($sqlPreguntas);

        foreach ($preguntas as $pregunta){
            $preguntaCampo = $pregunta->campo;
            $puntaje = 0;
            $puntaje_total = 0;
            if ($rs->$preguntaCampo == 2) {
                $puntaje = 0;
                $puntaje_total = $pregunta->puntaje;
            }
            else if ($rs->$preguntaCampo == 1) {
                $puntaje = $pregunta->puntaje;
                $puntaje_total = $pregunta->puntaje;
            }

            if ($puntaje_total >= 0){
                $respuesta = $rs->$preguntaCampo != "" ? $rs->$preguntaCampo : null;
                $datosConsolidadoDetalle = DatosConsolidadosDetalle::create([
                    'cod_estudio' => $rs->cod_estudio,
                    'cod_tarea' => $rs->cod_tarea,
                    'pregunta_id' => $pregunta->id,
                    'pregunta' => $preguntaCampo,
                    'respuesta' => $respuesta,
                    'puntaje' => $puntaje,
                    'puntaje_total' => $puntaje_total
                ]);
                // dd($datosConsolidadoDetalle);
            }
        }
    }


    public function pivot(Request $request){

        $validatedData = $request->validate([
            'estudio' => ['required'],
        ]);
        
        $estudio = $request->estudio && $request["estudio"] != null ? $request["estudio"] : null;
        // dd($estudio);

        $sql = "select pauta_id, pilars.id as pilar_id, pilars.descripcion, sum(puntaje) as sum_puntaje ";
        $sql .= "from preguntas, pilars  ";
        $sql .= "where preguntas.pilar_id = pilars.id GROUP by pauta_id, pilars.id, pilars.descripcion ";
        $sql .= "order by pauta_id, pilars.id";

        $preguntaTotales = DB::select($sql);

        $totales = [];
        foreach ($preguntaTotales as $preguntaTotal){
            if (! array_key_exists($preguntaTotal->pauta_id, $totales)){
                $totales[$preguntaTotal->pauta_id][0] = $preguntaTotal->sum_puntaje;
            }
            else {
                $totales[$preguntaTotal->pauta_id][0] += $preguntaTotal->sum_puntaje;
            }
            $totales[$preguntaTotal->pauta_id][$preguntaTotal->pilar_id] = $preguntaTotal->sum_puntaje;
        }

        $sql = "select * from datos_consolidados where cod_estudio = $estudio and status in (5, 6) ";
        $sql .= "and cod_tarea not in (select distinct cod_tarea from datos_consolidados_detalles where cod_estudio = $estudio)  limit 0, 200";

        // dd($sql);

        $datoConsolidados = DB::select($sql);

        foreach ($datoConsolidados as $datoConsolidado){
            $totalesEvaluacion = $totales;
            if ($estudio < 210818 &&  !is_null($datoConsolidado->P0_1)){
                $this->TotalesIsla($totalesEvaluacion, $datoConsolidado);
            }

            if ($estudio >= 210818 &&  !is_null($datoConsolidado->P1_1)){
                $this->TotalesIsla($totalesEvaluacion, $datoConsolidado);
            }

            if (!is_null($datoConsolidado->P0_2_2)){
                $this->TotalesTienda($totalesEvaluacion, $datoConsolidado);
            }

            if (!is_null($datoConsolidado->P0_3_2)){
                $this->TotalesLavado($totalesLavalado, $datoConsolidado);
            }
        }

        
        return "Fin";


    }

    public function getResumenCodEstudio(){
        $datosConsolidados = DatosConsolidadosDetalle::groupBy('cod_estudio')
            ->join('estudios', 'datos_consolidados_detalles.cod_estudio', 'estudios.id')
            ->select('descripcion', 'cod_estudio', DB::raw('count(DISTINCT cod_tarea) as total'))->get();
        //Usermeta::groupBy('browser')->select('browser', DB::raw('count(*) as total'))->get();

        return $this->sendResponse($datosConsolidados, 'Resumen Código de estudio');

    }
}
