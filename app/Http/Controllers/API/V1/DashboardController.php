<?php

namespace App\Http\Controllers\API\V1;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class DashboardController extends BaseController
{
    public function getTotales(Request $request){

        $where = aplicaFiltros($request);

        $sql = "select pautas.id, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0 and  ";
        $sql .= "pautas.id = 1 and  ";
        $sql .= "medicions.activo              $where  ";
        $sql .= "group by pautas.id, pautas.descripcion  ";
        
        $sql .= "union  ";
        
        $sql .= "select pautas.id, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and preguntas.puntaje > 0 and  ";
        $sql .= "pautas.id = 2 and  ";
        $sql .= "datos_consolidados.P0_2_2 = 1 and  ";
        $sql .= "medicions.activo           $where ";
        $sql .= "group by pautas.id, pautas.descripcion  ";
        
        $sql .= "union  ";
        
        $sql .= "select pautas.id, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0 and pautas.id = 3 and  ";
        $sql .= "datos_consolidados.P0_3_2 = 1 and  ";
        $sql .= "medicions.activo         $where ";
        $sql .= "group by pautas.id, pautas.descripcion";

        // dd($sql);

        $decimal = 2;
        
        $resultadoTotalFormato = [];
        $datos = DB::select($sql);
        foreach ($datos as $dato){
            $resultadoTotalFormato['pauta_'.$dato->id] = round($dato->porc, $decimal);
        }

        return $this->sendResponse($resultadoTotalFormato, 'Total por pauta');

    }

    // Gráfico
    public function getResumen(Request $request){
        
        $where = aplicaFiltros($request);

        $whereFiltroPerfil = aplicaFiltrosPerfil();
        
        $sql = "select pautas.id, medicions.id as medicion_id, medicions.descripcion_corto, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0 and  ";
        $sql .= "pautas.id = 1 and  ";
        $sql .= "medicions.activo            $where  $whereFiltroPerfil ";
        $sql .= "group by pautas.id, medicions.id, pautas.descripcion, medicions.descripcion_corto  ";

        $sql .= "union  ";
        
        $sql .= "select pautas.id, medicions.id as medicion_id, medicions.descripcion_corto, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and preguntas.puntaje > 0 and  ";
        $sql .= "pautas.id = 2 and  ";
        $sql .= "datos_consolidados.P0_2_2 = 1 and  ";
        $sql .= "medicions.activo               $where  $whereFiltroPerfil ";
        $sql .= "group by pautas.id, medicions.id, pautas.descripcion, medicions.descripcion_corto  ";
        
        $sql .= "union  ";
        
        $sql .= "select pautas.id, medicions.id as medicion_id, medicions.descripcion_corto, pautas.descripcion as pauta,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total, (sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total)) * 100 as porc,  ";
        $sql .= "count(distinct datos_consolidados_detalles.cod_tarea) as n  ";
        $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pautas, eds, estudios, medicions  ";
        $sql .= "where  ";
        $sql .= "datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pauta_id = pautas.id AND  ";
        $sql .= "datos_consolidados.eds_id = eds.id and  ";
        $sql .= "datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id and  ";
        $sql .= "preguntas.puntaje > 0 and pautas.id = 3 and  ";
        $sql .= "datos_consolidados.P0_3_2 = 1 and  ";
        $sql .= "medicions.activo         $where  $whereFiltroPerfil ";
        $sql .= "group by pautas.id, medicions.id, pautas.descripcion, medicions.descripcion_corto;";

        
        // dd($sql);
        $resultadoPorFormato = [];
        $labels = [];
        $totales = [];
        $datos = DB::select($sql);
        $primera = true;
        $valorResta = 0;
        $decimal = 2;

        foreach ($datos as $dato){
            if ($primera){
                $valorResta = $dato->medicion_id;
                $primera = false;
            }
            $resultadoPorFormato['formato_' . $dato->id][] = number_format($dato->porc, $decimal, '.', ',');
            $resultadoPorFormato['total_' . $dato->id][] = $dato->n;
            $labels[$dato->medicion_id-$valorResta] = $dato->descripcion_corto;
            
        }

        // dd($resultadoPorFormato);

        return $this->sendResponse(
            ['resumen' => $resultadoPorFormato,
            'labels' => $labels,  
            'totales' => $totales],  
            
            'Datos dashboard');


    }
}
