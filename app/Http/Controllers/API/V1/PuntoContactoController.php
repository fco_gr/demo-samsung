<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PuntoContactoController extends BaseController
{
    public function getDatos(Request $request){

        $where = aplicaFiltros($request);
        $whereFiltroPerfil = aplicaFiltrosPerfil();

        $decimales = 1;

        $sql = "select pautas.descripcion, sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sql .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total,  ";
        $sql .= "round(sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total) * 100, $decimales) as porc  ";
        $sql .= "from  ";
        $sql .= "datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, pautas, estudios, medicions  ";
        $sql .= "where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sql .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sql .= "preguntas.pauta_id = pautas.id and datos_consolidados.cod_estudio = estudios.id and  ";
        $sql .= "estudios.medicion_id = medicions.id      $where   $whereFiltroPerfil  ";
        $sql .= "group by pautas.descripcion";

        // dd($sql);
        $resumenPautas = DB::select($sql);
        // dd($resumenPautas);


        $sqlPilarTotal = "select pilars.id, pilars.descripcion as pilar,  ";
        $sqlPilarTotal .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sqlPilarTotal .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total,  ";
        $sqlPilarTotal .= "round(sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total) * 100, $decimales) as porc  ";
        $sqlPilarTotal .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, estudios, medicions  ";
        $sqlPilarTotal .= "where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sqlPilarTotal .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sqlPilarTotal .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sqlPilarTotal .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id  ";
        $sqlPilarTotal .= " $where   $whereFiltroPerfil ";
        $sqlPilarTotal .= "group by pilars.id, pilars.descripcion";

        

        $sqlPilarDetalle = "select pilars.id, pilars.descripcion, preguntas.campo, preguntas.descripcion, pilars.descripcion as pauta,  ";
        $sqlPilarDetalle .= "sum(datos_consolidados_detalles.puntaje) as puntaje,  ";
        $sqlPilarDetalle .= "sum(datos_consolidados_detalles.puntaje_total) as puntaje_total,  ";
        $sqlPilarDetalle .= "round(sum(datos_consolidados_detalles.puntaje) / sum(datos_consolidados_detalles.puntaje_total) * 100, $decimales) as porc  ";
        $sqlPilarDetalle .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pilars, eds, estudios, medicions  ";
        $sqlPilarDetalle .= "where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea AND  ";
        $sqlPilarDetalle .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND  ";
        $sqlPilarDetalle .= "preguntas.pilar_id = pilars.id AND datos_consolidados.eds_id = eds.id and  ";
        $sqlPilarDetalle .= "datos_consolidados.cod_estudio = estudios.id and estudios.medicion_id = medicions.id and  ";
        $sqlPilarDetalle .= "preguntas.puntaje > 0 and pilars.id = &&& $where   $whereFiltroPerfil ";
        $sqlPilarDetalle .= "group by pilars.id, pilars.descripcion, preguntas.campo, preguntas.descripcion, pilars.descripcion  ";



        // dd($sqlPilarTotal);
        $pilarTotals = DB::select($sqlPilarTotal);
        $resumenPilar = [];


        foreach ($pilarTotals as $pilarTotal){
            $resumenPilar[$pilarTotal->id]['total'] = $pilarTotal;
            $sqlDetalle = str_replace("&&&", $pilarTotal->id, $sqlPilarDetalle);
            $DatosDetalles = DB::select($sqlDetalle);
            $resumenPilar[$pilarTotal->id]['detalle'] = $DatosDetalles;

        }

        return $this->sendResponse(
            [
                'resumen_pautas' => $resumenPautas,
                'cuadro_pautas' => $resumenPilar
            ],  'Datos punto de contacto');

    }
}
