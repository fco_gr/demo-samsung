<?php

namespace App\Http\Controllers\API\V1;

use App\Estudio;
use Carbon\Carbon;
use App\DatosConsolidado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SyncController extends BaseController
{
    public function getEstudios(){
        $estudios = Estudio::with('medicion')->get();
        return $this->sendResponse($estudios, 'Estudios Doit');
    }

    public function copiaDatosDoit(Request $request){

        $validatedData = $request->validate([
            'estudio' => ['required'],
            'fecha_ini' => 'nullable|date',
            'fecha_fin' => 'nullable|date',
        ]);
        
        $estudio = $request->estudio && $request["estudio"] != null ? $request["estudio"] : null;
        $fecha_ini = $request && $request["fecha_ini"] != null ? Carbon::parse($request["fecha_ini"])->format('Y-m-d') : null;
        $fecha_fin = $request && $request["fecha_fin"] != null ? Carbon::parse($request["fecha_fin"])->format('Y-m-d') : null;

        // dd($estudio, $fecha_ini, $fecha_fin);
        
        $estudios = Estudio::where('id', $estudio)->get();
        // dd($estudios);

        foreach ($estudios as $estudio){
            $codEstudio = $estudio->id;
            $tablaDatos = $estudio->tabla;
            $tareasTodas = '';
            // $datosConsolidadosCargados = DatosConsolidado::select('cod_estudio')
            //     ->where('cod_estudio', $codEstudio)
            //     ->whereBetween('FEC_EVAL', [$fecha_ini, $fecha_fin])
            //     ->pluck('cod_estudio')->toArray();

            $datosConsolidadosCargados = DatosConsolidado::select('cod_tarea')->where('cod_estudio', $codEstudio);

            $whereFechaDoit = "";
            if ($fecha_ini && $fecha_fin){
                $datosConsolidadosCargados = $datosConsolidadosCargados->whereBetween('FEC_EVAL', [$fecha_ini, $fecha_fin]);
                $whereFechaDoit =  "and $tablaDatos.FEC_EVAL >= '$fecha_ini' and $tablaDatos.FEC_EVAL <= '$fecha_fin' ";
            }
            $datosConsolidadosCargados = $datosConsolidadosCargados->pluck('cod_tarea')->toArray();
            $tareasTodas = implode( ", ", $datosConsolidadosCargados );

            $sqlExisten = "";
            if ($tareasTodas != ''){
                $sqlExisten = "and tareas.cod_tarea not in ($tareasTodas)";
            }

            $campos = "$tablaDatos.*, nombre, nivel, pago, cod_estudio, cod_cuestionario, descripcion, cod_sucursal, "; 
            $campos .= "fecha_inicio, fecha_termino, horas, mins, status, fecha_asignacion, fecha_completacion, fecha_validacion, ";
            $campos .= "latitud_inicial, longitud_inicial, latitud_final, longitud_final, ubicacion_inicial, ubicacion_final, cod_sucursal % 1000 as eds_id, now() as created_at, null as updated_at";

            $sqlDooit = "select $campos from tareas, $tablaDatos where ";
            $sqlDooit .= "tareas.cod_tarea = $tablaDatos.cod_tarea and cod_estudio = $codEstudio and status in (5, 6) ";
            $sqlDooit .= $whereFechaDoit;
            $sqlDooit .= "$sqlExisten;";

            $datoDooits = DB::connection('mysql_connect_doit')->select($sqlDooit);
            

            foreach ($datoDooits as $datoDooit){
                $datoCarga = (array) $datoDooit;
                DatosConsolidado::create($datoCarga);
            }


       
        }

        return "fin";
    }

    
}
