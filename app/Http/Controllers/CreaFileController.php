<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreaFileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $files = DB::select('select * from crea_file');
        foreach ($files as $file){
            // dd($file);
            $user = User::create([
                'name' => $file->nombre,
                'username' => $file->username,
                'email' => $file->mail,
                'password' => Hash::make($file->password),
                'type' => 'file'
            ]);

            // dd($user);

        }


    }
}
