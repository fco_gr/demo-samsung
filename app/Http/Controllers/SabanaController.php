<?php

namespace App\Http\Controllers;

use App\Medicion;
use Illuminate\Http\Request;
use App\Exports\SabanaExport;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;

class SabanaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!Gate::allows('isSabana')) {
            return $this->unauthorizedResponse();
        }

        $medicions = Medicion::orderBy('fecha_numero', 'DESC')->get();
        // dd($medicions);
        return view('sabana.index', compact('medicions'));
    }


    public function export($medicion, $pauta) 
    {
        if ($pauta == 11){
            $pauta = 1;
            $medicion = "2, 3, 4";
        }
        else if ($pauta == 22){
            $pauta = 2;
            $medicion = "2, 3, 4";
        }
        else if ($pauta == 33){
            $pauta = 3;
            $medicion = "2, 3, 4";
        }
        $nombreSabana = '';
        if ($pauta == 1){
            $nombreSabana = "sabana_combustible.xlsx";
        }
        else if ($pauta == 2){
            $nombreSabana = "sabana_tienda.xlsx";
        }
        else if ($pauta == 3){
            $nombreSabana = "sabana_lavado.xlsx";
        }

        try {
            return Excel::download(new SabanaExport($medicion, $pauta), $nombreSabana);
        } catch (\Throwable $th) {
            // dd($th);
            return redirect('/home/dashboard');
        }
        
    }
}
