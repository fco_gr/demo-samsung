<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosConsolidado extends Model
{
    protected $fillable = [
        'cod_tarea', 'cod_dooer', 'fecha', 'FEC_EVAL', 'DIA', 'HORA_ENTRADA', 'HORA_SALIDA', 'HR_INICIO1', 'HR_FIN1', 'P0_1', 'P1_1', 
        'P1_1_1', 'P1_1_1_TXT', 'P1_2', 'P1_2_1', 'P1_2_1_TXT', 'P1_3', 'P1_3_1', 'P1_3_1_TXT', 'P1_4', 'P1_4_1', 'P1_4_1_TXT', 'P1_5', 
        'P1_5_TXT', 'P1_6', 'P1_6_TXT', 'P1_7', 'P1_7_TXT', 'P1_8', 'P1_8_1', 'P1_8_1_TXT', 'P1_8_TXT', 'P1_9', 'P1_9_TXT', 'P1_11', 
        'P1_11_1', 'P1_11_1_TX', 'P1_12', 'P1_13', 'P1_13_1', 'P1_13_1_TX', 'P1_14', 'P1_14_TXT', 'P1_15', 'P1_15_1', 'P1_15_1_TX', 
        'P1_15_A', 'P1_16', 'P1_16_1', 'P1_16_1_TX', 'P1_17', 'P1_17_1', 'P1_17_1_TX', 'FOTO_BOL1', 'P1_18', 'MEDIO_PAGO', 'NOTA1', 
        'NOTA1_TXT', 'P0_2_2', 'HR_INICIO2', 'HR_FIN2', 'P2_1', 'P2_1_TXT', 'P2_2', 'P2_2_TXT', 'P2_3', 'P2_3_TXT', 'P2_4', 'P2_4_1', 
        'P2_4_1_TXT', 'P2_5', 'P2_5_TXT', 'P2_6', 'P2_6_1', 'P2_6_1_TXT', 'P2_6_A', 'P2_7', 'P2_7_1', 'P2_7_1_TXT', 'P2_8', 'P2_8_1', 
        'P2_8_1_TXT', 'NOTA2', 'NOTA2_TXT', 'P0_2', 'P2_9', 'P2_9_TXT', 'P2_10', 'P2_10_TXT', 'P2_11', 'P2_11_1', 'P2_11_1_TX', 'P2_12', 
        'P2_12_TXT', 'P2_19', 'P0_2_1', 'P2_13', 'P2_13_1', 'P2_13_1_TX', 'P2_14', 'P2_14_1', 'P2_14_1_TX', 'FOTO_BANO', 'P2_15', 
        'P2_15_1', 'P2_15_1_TX', 'FOTO_BANO2', 'P2_16', 'P2_16_1', 'P2_16_1_TX', 'FOTO_BANO3', 'FOTO_BOL2', 'P2_18', 'P0_3_2', 
        'HR_INICIO3', 'HR_FIN3', 'P0_3', 'P3_2', 'P3_2_TXT', 'P3_1', 'P1_3_TXT', 'P3_3', 'P3_3_TXT', 'P0_3_1', 'P3_5', 'P3_5_1', 
        'P3_5_1_TXT', 'P3_6', 'P3_6_TXT', 'P3_7', 'P3_7_TXT', 'P3_8', 'P3_8_TXT', 'P3_9', 'P3_9_TXT', 'P3_10', 'P3_10_TXT', 'P3_11', 
        'P3_11_TXT', 'P3_12', 'P3_12_TXT', 'P3_13', 'P3_13_1', 'P3_13_1_TX', 'P3_13_A', 'P3_14', 'P3_14_1', 'P3_14_1_TX', 'P3_15', 
        'P3_15_1', 'P3_15_1_TX', 'FOTO_BOL3', 'P3_16', 'NOTA3', 'NOTA3_TXT', 'OBS_GENE', 'nombre', 'nivel', 'pago', 'cod_estudio', 
        'cod_cuestionario', 'descripcion', 'cod_sucursal', 'fecha_inicio', 'fecha_termino', 'horas', 'mins', 'status', 'fecha_asignacion', 
        'fecha_completacion', 'fecha_validacion', 'latitud_inicial', 'longitud_inicial', 'latitud_final', 'longitud_final', 
        'ubicacion_inicial', 'ubicacion_final', 'eds_id', 'created_at', 'updated_at',
        
        'P1_6_1', 'P1_8_2', 'P1_9_1', 'P1_10', 'P1_10_1', 'P1_10_TXT', 'P1_22', 'P1_22_TXT', 'P1_19', 'P1_19_TXT', 'P1_20', 
        'P1_21', 'P1_21_1', 'P1_21_TXT', 'P1_12_TXT', 'P2_20', 'P2_20_TXT', 'P3_4', 'P3_4_1', 'P3_4_TXT', 'P3_17', 'P3_17_TXT'

    ];
}
