<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eds extends Model
{
    protected $fillable = [
        'id', 'razon_social', 'rut_rz', 'direccion', 'comuna', 'region', 'jefe_zona', 'subgerente_area', 
        'plataforma', 'plataforma_tienda', 'imagen_tienda', 'j_zona_tienda', 'jefe_lavado', 's_zona_tienda', 
        'subgerente_area_nombre', 'subgerente_area_mail', 'jefe_de_zona_nombre', 'jefe_de_zona_mail', 'jefe_de_tienda_nombre', 
        'jefe_de_tienda_mail', 'jefe_operacion_tienda_nombre', 'jefe_operacion_tienda_mail', 'origen'
    ];
}
