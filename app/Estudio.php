<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
    /**
     * Get the user that owns the Tarea
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medicion()
    {
        return $this->belongsTo(Medicion::class);
    }
}
