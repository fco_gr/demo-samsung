<?php

use App\Eds;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

// composer dump-autoload 

if (!function_exists('aplicaFiltros')) {
    function aplicaFiltros(Request $request)
    {
        if ($request->all() == []){
            return '';
        }

        $plataformas = $request->filtro ? $request->filtro["plataforma_id"] : [];
        $meses = $request->filtro ? $request->filtro["meses_id"] : [];
        $unidades = $request->filtro ? $request->filtro["unidad_id"] : [];
        $negocios = $request->filtro ? $request->filtro["negocios_id"] : [];
        $files = $request->filtro ? $request->filtro["file_id"] : [];
        // dd($request->all());

        $fecha_ini = $request->filtro && $request->filtro["fecha_ini"] != null ? Carbon::parse($request->filtro["fecha_ini"])->format('Y-m-d') : null;
        $fecha_fin = $request->filtro && $request->filtro["fecha_fin"] != null ? Carbon::parse($request->filtro["fecha_fin"])->format('Y-m-d') : null;
        // dd($fecha_ini, $fecha_fin);

        $sWhere = "";
        
        if (count($plataformas) > 0){
            $aCodigos = [];
            foreach ($plataformas as $plataforma){
                $aCodigos[] = $plataforma['code'];
            }
            $aCodigos = implode("', '", $aCodigos);
            $sWhere .= " and eds.plataforma in ('$aCodigos') ";
        }

        if (count($meses) > 0){
            $aCodigos = [];
            foreach ($meses as $mes){
                $aCodigos[] = $mes['code'];
            }
            $aCodigos = implode(", ", $aCodigos);
            $sWhere .= " and medicions.id in ($aCodigos) ";
        }

        if (count($negocios) > 0){
            $aCodigos = [];
            foreach ($negocios as $negocio){
                $aCodigos[] = $negocio['code'];
            }
            $aCodigos = implode(", ", $aCodigos);
            $sWhere .= " and preguntas.pauta_id in ($aCodigos) ";
            // dd($sWhere) ;
        }

        if (count($files) > 0){
            $aCodigos = [];
            foreach ($files as $file){
                // dd($file);
                $aCodigos[] = $file;
            }
            $aCodigos = implode(", ", $aCodigos);
            // dd($aCodigos);
            $sWhere .= " and eds.id in ($aCodigos) ";
            // dd($sWhere) ;
        }

        // if ($fecha_ini && $fecha_fin){
        //     $sWhere .= " and (FEC_EVAL >= '$fecha_ini' and FEC_EVAL <= '$fecha_fin') ";
        // }
        
        // dd($sWhere);

        return $sWhere;


    }
}


if (!function_exists('aplicaFiltrosPerfil')) {
    function aplicaFiltrosPerfil()
    {
        $emailPerfil = Auth::user()->email;
        $whereFiltroPerfil = "";
        if (Gate::allows('isSubGerente')) {
            $whereFiltroPerfil = " and eds.subgerente_area_mail = '$emailPerfil' ";
        }
        else if (Gate::allows('isJefeZona')) {
            $whereFiltroPerfil = " and eds.jefe_de_zona_mail = '$emailPerfil' ";
        }
        else if (Gate::allows('isJefeOperacion')) {
            $whereFiltroPerfil = " and eds.jefe_operacion_tienda_mail = '$emailPerfil' ";
        }
        else if (Gate::allows('isJefeTienda')) {
            $whereFiltroPerfil = " and eds.jefe_de_tienda_mail = '$emailPerfil' ";
        }
        else if (Gate::allows('isFile')) {
            $fileId = str_replace('file', '', Auth::user()->username);
            $whereFiltroPerfil = " and eds.id = $fileId ";
        }

        if (Gate::allows('isFile')) {
            $fileId = str_replace('file', '', Auth::user()->username);
            $whereFiltroPerfil = " and eds.id = $fileId ";
        }

        return $whereFiltroPerfil;


    }
}

if (!function_exists('aplicaFiltrosPerfilBySubGerente')) {
    function aplicaFiltrosPerfilBySubGerente()
    {
        $emailPerfil = Auth::user()->email;
        $whereFiltroPerfil = "";

        
        
        // if (Gate::allows('isSubGerente')) {
        //     $whereFiltroPerfil = " and eds.subgerente_area_mail = '$emailPerfil' ";
        // }
        // else 
        if (Gate::allows('isJefeZona')) {
            $subGerente = Eds::where('jefe_de_zona_mail', $emailPerfil)->first();
            // dd($subGerente->subgerente_area_mail);

            $whereFiltroPerfil = " and eds.subgerente_area_mail = '$subGerente->subgerente_area_mail' ";
        }
        

        return $whereFiltroPerfil;


    }
}

if (!function_exists('fechaTramo')) {
    function fechaTramo($fecha){

        $meses_espanol = array(
            '1' => 'Enero',
            '2' => 'Febrero',
            '3' => 'Marzo',
            '4' => 'Abril',
            '5' => 'Mayo',
            '6' => 'Junio',
            '7' => 'Julio',
            '8' => 'Agosto',
            '9' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        $fechaComoEntero = strtotime($fecha);
        $anio = date("Y", $fechaComoEntero);
        $mes = (int)date("m", $fechaComoEntero);
        $dia = (int)date("d", $fechaComoEntero);
        $CantidadDeDias = date("t", $fechaComoEntero);

        $fechaEnTramo = '';
        if ($dia >= 1 && $dia <= 7){
            $fechaEnTramo = "01 al 07 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 8 && $dia <= 14){
            $fechaEnTramo = "08 al 14 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 15 && $dia <= 21){
            $fechaEnTramo = "15 al 21 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 16){
            $fechaEnTramo = "22 al " . $CantidadDeDias . " de " . $meses_espanol[$mes];
        }
        return $fechaEnTramo;
    }
}

if (!function_exists('mesInicial')) {
    function mesInicial(){
        $aFecha = [ 
            "name" => "Noviembre 2021", 
            "code" => "6" 
        ];

        $arrayobject = new ArrayObject($aFecha);
        return [$arrayobject];
    }
}