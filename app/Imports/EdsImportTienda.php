<?php

namespace App\Imports;

use App\Eds;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EdsImportTienda implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            // dd($row);
            $eds = Eds::updateOrCreate(
                [
                    'id' => $row['file']
                ],
                [
                    'plataforma_tienda' => $row['operacion'], 
                    'imagen_tienda' => $row['formato'], 
                    'jefe_de_tienda_nombre' => $row['jefe_zona_tienda'], 
                    'jefe_de_tienda_mail' => $row['mail_jzo_tienda'], 
                    'jefe_operacion_tienda_nombre' => $row['jefe_operacion_tienda'], 
                    'jefe_operacion_tienda_mail' => $row['mail_operaciones'], 
                ]
                
            );
        }
    }
}
