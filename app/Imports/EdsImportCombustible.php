<?php

namespace App\Imports;

use App\Eds;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EdsImportCombustible implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            // dd($row);
            $eds = Eds::updateOrCreate(
                [
                    'id' => $row['file']
                ],
                [
                    'direccion' => $row['direccion'], 
                    'comuna' => $row['comuna'], 
                    'region' => $row['region'], 
                    'plataforma' => $row['plataforma_operacion'], 
                    'subgerente_area_nombre' => $row['subgerente'], 
                    'subgerente_area_mail' => $row['mail_subgerente'], 
                    'jefe_de_zona_nombre' => $row['jefe_de_zona'], 
                    'jefe_de_zona_mail' => $row['mail_jzo'], 
                ]
                
            );
        }
    }
}
