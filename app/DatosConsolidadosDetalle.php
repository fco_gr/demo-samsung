<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosConsolidadosDetalle extends Model
{
    protected $fillable = [
        'cod_estudio', 'cod_tarea', 'pregunta_id', 'pregunta', 'respuesta', 'puntaje', 'puntaje_total'
    ];
}
