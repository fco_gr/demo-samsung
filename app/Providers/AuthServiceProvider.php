<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**
         * Defining the user Roles
         */
        Gate::define('isAdmin', function ($user) {
            // if ($user->isAdmin()) {
            //     return true;
            // }

            // for simplicity
            return $user->type === 'admin';
        });

        Gate::define('isUser', function ($user) {
            // if ($user->isUser()) {
            //     return true;
            // }

            // for simplicity
            return $user->type === 'user';
        });

        Gate::define('isGerente', function ($user) {
            return $user->type === 'gerente';
        });

        Gate::define('isGerenteSinBiblioteca', function ($user) {
            return $user->type === 'gerente_sin_biblio';
        });

        Gate::define('isSubGerente', function ($user) {
            return $user->type === 'sg_area';
        });

        Gate::define('isJefeZona', function ($user) {
            return $user->type === 'j_zona';
        });

        Gate::define('isJefeTienda', function ($user) {
            return $user->type === 'j_tienda';
        });

        Gate::define('isJefeOperacion', function ($user) {
            return $user->type === 'j_opera';
        });

        Gate::define('isFile', function ($user) {
            return $user->type === 'file';
        });

        Gate::define('isSabana', function ($user) {
            return $user->type === 'admin' || $user->type === 'gerente' || 
                $user->type === 'sg_area' || $user->type === 'j_zona' || 
                $user->type === 'j_tienda' || $user->type === 'j_opera' || 
                $user->type === 'gerente_sin_biblio';
        });

        Gate::define('isBiblioteca', function ($user) {
            return $user->type === 'admin' || $user->type === 'gerente';
        });


        Passport::routes();
    }
}
