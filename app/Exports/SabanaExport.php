<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class SabanaExport implements FromView, WithColumnFormatting, WithColumnWidths
{

    public function columnWidths(): array
    {

        $arrayWidths = [];
        if ($this->pauta == 1){
            $arrayWidths = [
                'A' => 15,
                'B' => 15,
                'H' => 20,
                'I' => 15,
                'K' => 20,
                'L' => 20,
                'M' => 20,
                'N' => 20,
                'O' => 15,
            ];
        }
        else if ($this->pauta == 2){
            $arrayWidths = [
                'A' => 15,
                'B' => 15,
                'G' => 15,
                'I' => 25,
                'J' => 20,
                'L' => 20,
                'M' => 20,
                'N' => 20,
                'O' => 20,
                'P' => 15,
            ];
        }
        else if ($this->pauta == 3){
            $arrayWidths = [
                'A' => 15,
                'B' => 15,
                'H' => 25,
                'I' => 15,
                'K' => 20,
                'L' => 20,
                'M' => 30,
                'O' => 15,
            ];
        }

        return $arrayWidths;
    }

    public function columnFormats(): array
    {
        $arrayFormat = [];
        if ($this->pauta == 1){
            $arrayFormat = [
                'T' => NumberFormat::FORMAT_PERCENTAGE,
                'W' => NumberFormat::FORMAT_PERCENTAGE,
                'AC' => NumberFormat::FORMAT_PERCENTAGE,
                'AJ' => NumberFormat::FORMAT_PERCENTAGE,
                'AT' => NumberFormat::FORMAT_PERCENTAGE,
                'BF' => NumberFormat::FORMAT_PERCENTAGE,
                'BH' => NumberFormat::FORMAT_PERCENTAGE,
            ];
        }
        else if ($this->pauta == 2){
            $arrayFormat = [
                'U' => NumberFormat::FORMAT_PERCENTAGE,
                'X' => NumberFormat::FORMAT_PERCENTAGE,
                'AA' => NumberFormat::FORMAT_PERCENTAGE,
                'AF' => NumberFormat::FORMAT_PERCENTAGE,
                'AM' => NumberFormat::FORMAT_PERCENTAGE,
                'AT' => NumberFormat::FORMAT_PERCENTAGE,
                'BD' => NumberFormat::FORMAT_PERCENTAGE,
                'BF' => NumberFormat::FORMAT_PERCENTAGE,
                
            ];
        }
        else if ($this->pauta == 3){
            $arrayFormat = [
                'S' => NumberFormat::FORMAT_PERCENTAGE,
                'V' => NumberFormat::FORMAT_PERCENTAGE,
                'X' => NumberFormat::FORMAT_PERCENTAGE,
                'AA' => NumberFormat::FORMAT_PERCENTAGE,
                'AK' => NumberFormat::FORMAT_PERCENTAGE,
                'AS' => NumberFormat::FORMAT_PERCENTAGE,
                'AU' => NumberFormat::FORMAT_PERCENTAGE,
                
            ];
        }
        
        return $arrayFormat;
    }

    private function tramoHorario($hora){
        $tramo  = "";
        if ($hora >= 7 && $hora <=13){
            $tramo = "8:00 - 13:59";
        }
        else {
            $tramo = "14:00 - 20:59";
        }

        return $tramo;
    } 

    private function fechaTramo($fecha){

        $meses_espanol = array(
            '1' => 'Enero',
            '2' => 'Febrero',
            '3' => 'Marzo',
            '4' => 'Abril',
            '5' => 'Mayo',
            '6' => 'Junio',
            '7' => 'Julio',
            '8' => 'Agosto',
            '9' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );


        $fechaComoEntero = strtotime($fecha);
        $anio = date("Y", $fechaComoEntero);
        $mes = (int)date("m", $fechaComoEntero);
        $dia = (int)date("d", $fechaComoEntero);
        $CantidadDeDias = date("t", $fechaComoEntero);

        $fechaEnTramo = '';
        if ($dia >= 1 && $dia <= 7){
            $fechaEnTramo = "01 al 07 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 8 && $dia <= 14){
            $fechaEnTramo = "08 al 14 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 15 && $dia <= 21){
            $fechaEnTramo = "15 al 21 de " . $meses_espanol[$mes];
        }
        else if ($dia >= 16){
            $fechaEnTramo = "22 al " . $CantidadDeDias . " de " . $meses_espanol[$mes];
        }
        return $fechaEnTramo;
    }

    private function recuperaRespuestas($respuestas, $campo, $valor){
        if ($valor != ''){
            $lfcr = chr(13).chr(13);
            $listaValores = explode(",", $valor);
            $texto = '';
            try {
                for ($i=0; $i<count($listaValores); $i++){
                    $texto .= $listaValores[$i] .' '. $respuestas[$campo][$listaValores[$i]];
                    if ($i != count($listaValores) - 1){
                        $texto .= $lfcr;
                    }
                }
            } catch (\Throwable $th) {
                dd($respuestas, $campo, $valor);
            }
            
            return $texto;
        }
        return $valor;
    }

    public function __construct(string $medicion, int $pauta)
    {
        $this->medicion = $medicion;
        $this->pauta = $pauta;
    }

    public function view(): View
    {

        // Combustibles
        
        // $whereFiltroPerfil = aplicaFiltrosPerfil();
        $whereFiltroPerfil = '';
        // dd($whereFiltroPerfil);
        
        if ($this->pauta == 1){
            $sql = "select datos_consolidados.cod_tarea, medicions.agno as agno, medicions.mes, eds.id, estudios.descripcion, eds.plataforma, eds.direccion, eds.comuna, eds.region,  cod_dooer, VISITA, ";
            $sql .= "eds.jefe_de_zona_nombre as jefe_zona, eds.subgerente_area, P1_15_A as nom_colaborador, DATE_FORMAT(datos_consolidados.FEC_EVAL, '%d-%m-%Y') as fecha, datos_consolidados.HR_INICIO1 as horallegada,  ";
            $sql .= "if (totales.totales.puntaje_total > 0, (totales.totales.puntaje / totales.totales.puntaje_total) * 100, null) as resultado_final,   ";
            $sql .= "totales.puntaje as puntaje_obtenido, totales.puntaje_total as puntaje_total,   ";
            $sql .= "if (totales.bienvenida_puntaje_total > 0, (totales.bienvenida_puntaje / totales.bienvenida_puntaje_total) * 100, null) as puntaje_bienvenida_total,  ";
            
            $sql .= "NOTA1, P1_17, P1_17_1, P1_22, P1_9_1, P1_21, P1_21_1, P1_6_1, P1_8_2, P1_10, P1_10_1, P1_19, P1_12, ";

            $sql .= "P1_2, P1_2_1, P1_3, P1_3_1, P1_5,  ";
            $sql .= "if (totales.rapidez_puntaje_total > 0, (totales.rapidez_puntaje / totales.rapidez_puntaje_total) * 100, null) as puntaje_rapidez_total,  ";
            $sql .= "P1_1, P1_1_1, P1_4, P1_4_1, P1_13, P1_13_1,  ";
            $sql .= "if (totales.confianza_puntaje_total > 0, (totales.confianza_puntaje / totales.confianza_puntaje_total) * 100, null) as puntaje_confianza_total,  ";
            $sql .= "P1_7, P1_9, P1_15, P1_15_1, P1_16, P1_16_1,  ";
            $sql .= "if (totales.actitud_puntaje_total > 0, (totales.actitud_puntaje / totales.actitud_puntaje_total) * 100, null) as puntaje_actitud_total,  ";
            $sql .= "P1_6, P1_8, P1_8_1, P1_11, P1_11_1, P1_14, P1_17, P1_17_1, P1_18, metas.meta * 100 as meta_final,  ";
            $sql .= "if (metas.meta > 0, (totales.puntaje / totales.puntaje_total) / metas.meta * 100, 0) as resultado_final_v2  ";
            $sql .= "from datos_consolidados join (  ";
            $sql .= "    select datos_consolidados.cod_tarea,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje, 0)) as bienvenida_puntaje,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) as bienvenida_puntaje_total,  ";
            $sql .= "    sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje, 0)) as actitud_puntaje,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) as actitud_puntaje_total,  ";
            $sql .= "    sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje, 0)) as confianza_puntaje,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) as confianza_puntaje_total,  ";
            $sql .= "    sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje, 0)) as rapidez_puntaje,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) as rapidez_puntaje_total,  ";
            $sql .= "    sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje, 0)) as banos_puntaje,   ";
            $sql .= "    sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) as banos_puntaje_total,  ";
            $sql .= "    sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total   ";
            $sql .= "    from datos_consolidados, datos_consolidados_detalles, preguntas, pilars  ";
            $sql .= "    where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and   ";
            $sql .= "    datos_consolidados_detalles.pregunta_id = preguntas.id AND preguntas.pilar_id = pilars.id and pauta_id=1 and preguntas.puntaje > 0  and P0_1 is not null  ";
            $sql .= "    group by datos_consolidados.cod_tarea  ";
            $sql .= ") as totales, estudios, medicions, eds, metas   ";
            $sql .= "where datos_consolidados.cod_tarea = totales.cod_tarea and datos_consolidados.cod_estudio = estudios.id AND  ";
            $sql .= "datos_consolidados.eds_id = eds.id and estudios.medicion_id = medicions.id and medicions.agno = metas.agno and   ";
            $sql .= "medicions.trimestre = metas.trimestre and eds.plataforma = metas.plataforma and  ";
            $sql .= "P0_1 is not null and medicions.activo and                                 medicions.id in ($this->medicion)    $whereFiltroPerfil  ";
        }
        else if ($this->pauta == 2){
            $sql = "select datos_consolidados.cod_tarea, medicions.agno as agno, medicions.mes, eds.id, estudios.descripcion, eds.plataforma_tienda as plataforma, imagen_tienda, eds.direccion, eds.comuna, eds.region,    cod_dooer,  VISITA, ";
            $sql .= "eds.jefe_de_tienda_nombre as jefe_zona, eds.jefe_operacion_tienda_nombre as subgerente_area, P2_6_A as nom_colaborador, DATE_FORMAT(datos_consolidados.FEC_EVAL, '%d-%m-%Y') as fecha, datos_consolidados.HR_INICIO2 as horallegada, ";
            $sql .= "if (totales.puntaje_total > 0, (totales.puntaje / totales.puntaje_total) * 100, null) as resultado_final,  ";
            $sql .= "totales.puntaje as puntaje_obtenido, totales.puntaje_total as puntaje_total,  ";
            $sql .= "if (totales.bienvenida_puntaje_total > 0, (totales.bienvenida_puntaje / totales.bienvenida_puntaje_total) * 100, null) as puntaje_bienvenida_total, ";
            $sql .= "NOTA2, P2_8, P2_8_1, P2_19, P2_20,  ";
            $sql .= "P2_2, P2_9,  ";
            $sql .= "if (rapidez_puntaje_total > 0, (rapidez_puntaje / rapidez_puntaje_total) * 100, null) as puntaje_rapidez_total, ";
            $sql .= "P2_1, P2_4, P2_4_1, P2_10,  ";
            $sql .= "if (confianza_puntaje_total > 0, (confianza_puntaje / confianza_puntaje_total) * 100, null) as puntaje_confianza_total, ";
            $sql .= "P2_6, P2_6_1, P2_7, P2_7_1, P2_11, P2_11_1, ";
            $sql .= "if (actitud_puntaje_total > 0, (actitud_puntaje / actitud_puntaje_total) * 100, null) as puntaje_actitud_total, ";
            $sql .= "P2_3, P2_5, P2_8, P2_8_1, P2_12,  ";
            $sql .= "if (banos_puntaje_total > 0, (banos_puntaje / banos_puntaje_total) * 100, null) as puntaje_banos_total, ";
            $sql .= "P0_2_1, P2_13, P2_13_1, P2_14, P2_14_1, P2_15, P2_15_1, P2_16, P2_16_1,  ";
            $sql .= "P2_18, metas.meta * 100 as meta_final, ";
            $sql .= "if (metas.meta > 0, (totales.puntaje / totales.puntaje_total) / metas.meta * 100, 0) as resultado_final_v2 ";
            $sql .= "from datos_consolidados join ( ";
            $sql .= "select datos_consolidados.cod_tarea,  ";
            $sql .= "sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje, 0)) as bienvenida_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) as bienvenida_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje, 0)) as actitud_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) as actitud_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje, 0)) as confianza_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) as confianza_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje, 0)) as rapidez_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) as rapidez_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje, 0)) as banos_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) as banos_puntaje_total, ";
            $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total  ";
            $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pilars ";
            $sql .= "where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
            $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND preguntas.pilar_id = pilars.id and pauta_id=2 and preguntas.puntaje > 0 and P0_2_2 = 1 ";
            $sql .= "group by datos_consolidados.cod_tarea ";
            $sql .= ") as totales, estudios, medicions, eds, metas  ";
            $sql .= "where datos_consolidados.cod_tarea = totales.cod_tarea and datos_consolidados.cod_estudio = estudios.id AND ";
            $sql .= "datos_consolidados.eds_id = eds.id and estudios.medicion_id = medicions.id and medicions.agno = metas.agno and  ";
            $sql .= "medicions.trimestre = metas.trimestre and eds.plataforma_tienda = metas.plataforma and ";
            $sql .= "P0_2_2 = 1  and medicions.activo                  and medicions.id in ($this->medicion)   $whereFiltroPerfil   order by eds.id; ";
        }
        else if ($this->pauta == 3){
            $sql = "select datos_consolidados.cod_tarea, medicions.agno as agno, medicions.mes, eds.id, estudios.descripcion, eds.plataforma as plataforma, imagen_tienda, eds.direccion, eds.comuna, eds.region,    cod_dooer,  VISITA, ";
            $sql .= "eds.jefe_lavado as jefe_zona, eds.subgerente_area as subgerente_area, P3_13_A as nom_colaborador, DATE_FORMAT(datos_consolidados.FEC_EVAL, '%d-%m-%Y') as fecha, datos_consolidados.HR_INICIO3 as horallegada, ";
            $sql .= "if (totales.puntaje_total > 0, (totales.puntaje / totales.puntaje_total) * 100, null) as resultado_final,  ";
            $sql .= "totales.puntaje as puntaje_obtenido, totales.puntaje_total as puntaje_total,  ";
            $sql .= "if (totales.bienvenida_puntaje_total > 0, (totales.bienvenida_puntaje / totales.bienvenida_puntaje_total) * 100, null) as puntaje_bienvenida_total, ";
            $sql .= "NOTA3, P3_15, P3_15_1, P3_17,  ";
            $sql .= "P3_3,  ";
            $sql .= "if (rapidez_puntaje_total > 0, (rapidez_puntaje / rapidez_puntaje_total) * 100, null) as puntaje_rapidez_total, ";
            $sql .= "P3_2, P3_1, ";
            $sql .= "if (confianza_puntaje_total > 0, (confianza_puntaje / confianza_puntaje_total) * 100, null) as puntaje_confianza_total, ";
            $sql .= "P3_5, P3_5_1, P3_7, P3_9, P3_10, P3_13, P3_13_1, P3_14, P3_14_1,  ";
            $sql .= "if (actitud_puntaje_total > 0, (actitud_puntaje / actitud_puntaje_total) * 100, null) as puntaje_actitud_total, ";
            $sql .= "P3_6, P3_8, P3_11, P3_12, P3_15, P3_15_1,  ";
            $sql .= "p1_18, metas.meta * 100 as meta_final, ";
            $sql .= "if (metas.meta > 0, (totales.puntaje / totales.puntaje_total) / metas.meta * 100, 0) as resultado_final_v2 ";
            $sql .= "from datos_consolidados join ( ";
            $sql .= "select datos_consolidados.cod_tarea,  ";
            $sql .= "sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje, 0)) as bienvenida_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 1, datos_consolidados_detalles.puntaje_total, 0)) as bienvenida_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje, 0)) as actitud_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 2, datos_consolidados_detalles.puntaje_total, 0)) as actitud_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje, 0)) as confianza_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 3, datos_consolidados_detalles.puntaje_total, 0)) as confianza_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje, 0)) as rapidez_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 4, datos_consolidados_detalles.puntaje_total, 0)) as rapidez_puntaje_total, ";
            $sql .= "sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje, 0)) as banos_puntaje,  ";
            $sql .= "sum(if (preguntas.pilar_id = 5, datos_consolidados_detalles.puntaje_total, 0)) as banos_puntaje_total, ";
            $sql .= "sum(datos_consolidados_detalles.puntaje) as puntaje, sum(datos_consolidados_detalles.puntaje_total) as puntaje_total  ";
            $sql .= "from datos_consolidados, datos_consolidados_detalles, preguntas, pilars ";
            $sql .= "where datos_consolidados.cod_tarea = datos_consolidados_detalles.cod_tarea and  ";
            $sql .= "datos_consolidados_detalles.pregunta_id = preguntas.id AND preguntas.pilar_id = pilars.id and pauta_id=3 and preguntas.puntaje > 0 and P0_3_2 = 1 ";
            $sql .= "group by datos_consolidados.cod_tarea ";
            $sql .= ") as totales, estudios, medicions, eds, metas  ";
            $sql .= "where datos_consolidados.cod_tarea = totales.cod_tarea and datos_consolidados.cod_estudio = estudios.id AND ";
            $sql .= "datos_consolidados.eds_id = eds.id and estudios.medicion_id = medicions.id and medicions.agno = metas.agno and  ";
            $sql .= "medicions.trimestre = metas.trimestre and eds.plataforma = metas.plataforma and ";
            $sql .= "P0_3_2 = 1  and medicions.activo             and medicions.id in ($this->medicion)   $whereFiltroPerfil   order by eds.id; ";
        }
        // dd($sql);
        
        $sabanas = DB::select($sql);

        $respuestas = [];
        $respuestasDBs = DB::select("select * from respuestas where pauta_id=$this->pauta  order by id");


        foreach ($respuestasDBs as $respuestasDB){
            $respuestas[$respuestasDB->campo][$respuestasDB->valor] = $respuestasDB->descripcion;
        }

        $vista = '';
        $nombreXLS = '';
        foreach($sabanas as $sabana){
            if ($this->pauta == 1){
                $vista = "exports.sabanas";
                $sabana->fecha_tramo = $this->FechaTramo($sabana->fecha); 
                $sabana->horallegada_tramo =  $this->tramoHorario($sabana->horallegada);

                if ($sabana->P1_2_1 != ''){
                    $sabana->P1_2_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_2_1', $sabana->P1_2_1);
                } else {
                    $sabana->P1_2_1_detalle = '';
                }

                if ($sabana->P1_3_1 != ''){
                    $sabana->P1_3_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_3_1', $sabana->P1_3_1);
                } else {
                    $sabana->P1_3_1_detalle = '';
                }

                if ($sabana->P1_1_1 != ''){
                    $sabana->P1_1_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_1_1', $sabana->P1_1_1);
                } else {
                    $sabana->P1_1_1_detalle = '';
                }

                if ($sabana->P1_4_1 != ''){
                    $sabana->P1_4_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_4_1', $sabana->P1_4_1);
                } else {
                    $sabana->P1_4_1_detalle = '';
                }

                if ($sabana->P1_9_1 != ''){
                    $sabana->P1_9_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_9_1', $sabana->P1_9_1);
                } else {
                    $sabana->P1_9_1_detalle = '';
                }

                if ($sabana->P1_10_1 != ''){
                    $sabana->P1_10_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_10_1', $sabana->P1_10_1);
                } else {
                    $sabana->P1_10_1_detalle = '';
                }

                if ($sabana->P1_13_1 != ''){
                    $sabana->P1_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_13_1', $sabana->P1_13_1);
                } else {
                    $sabana->P1_13_1_detalle = '';
                }

                if ($sabana->P1_15_1 != ''){
                    $sabana->P1_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_15_1', $sabana->P1_15_1);
                } else {
                    $sabana->P1_15_1_detalle = '';
                }

                if ($sabana->P1_6_1 != ''){
                    $sabana->P1_6_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_6_1', $sabana->P1_6_1);
                } else {
                    $sabana->P1_6_1_detalle = '';
                }

                if ($sabana->P1_16_1 != ''){
                    $sabana->P1_16_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_16_1', $sabana->P1_16_1);
                } else {
                    $sabana->P1_16_1_detalle = '';
                }
                
                if ($sabana->P1_17_1 != ''){
                    $sabana->P1_17_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_17_1', $sabana->P1_17_1);
                } else {
                    $sabana->P1_17_1_detalle = '';
                }


                if ($sabana->P1_8_1 != ''){
                    $sabana->P1_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_8_1', $sabana->P1_8_1);
                } else {
                    $sabana->P1_8_1_detalle = '';
                }

                if ($sabana->P1_8_2 != ''){
                    $sabana->P1_8_2_detalle = $this->recuperaRespuestas($respuestas, 'P1_8_2', $sabana->P1_8_2);
                } else {
                    $sabana->P1_8_2_detalle = '';
                }

                if ($sabana->P1_11_1 != ''){
                    $sabana->P1_11_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_11_1', $sabana->P1_11_1);
                }
                else {
                    $sabana->P1_11_1_detalle = '';
                }

                if ($sabana->P1_17_1 != ''){
                    $sabana->P1_17_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_17_1', $sabana->P1_17_1);
                } else {
                    $sabana->P1_17_1_detalle = '';
                }

                if ($sabana->P1_21_1 != ''){
                    $sabana->P1_21_1_detalle = $this->recuperaRespuestas($respuestas, 'P1_21_1', $sabana->P1_21_1);
                } else {
                    $sabana->P1_21_1_detalle = '';
                }

            }
            else if ($this->pauta == 2){
                $vista = "exports.sabana_tienda";
                $sabana->fecha_tramo = $this->FechaTramo($sabana->fecha); 
                $sabana->horallegada_tramo =  $this->tramoHorario($sabana->horallegada);

                if ($sabana->P2_4_1 != ''){
                    $sabana->P2_4_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_4_1', $sabana->P2_4_1);
                } else {
                    $sabana->P2_4_1_detalle = '';
                }

                if ($sabana->P2_6_1 != ''){
                    $sabana->P2_6_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_6_1', $sabana->P2_6_1);
                } else {
                    $sabana->P2_6_1_detalle = '';
                }

                if ($sabana->P2_7_1 != ''){
                    $sabana->P2_7_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_7_1', $sabana->P2_7_1);
                } else {
                    $sabana->P2_7_1_detalle = '';
                }

                if ($sabana->P2_8_1 != ''){
                    $sabana->P2_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_8_1', $sabana->P2_8_1);
                } else {
                    $sabana->P2_8_1_detalle = '';
                }

                if ($sabana->P2_11_1 != ''){
                    $sabana->P2_11_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_11_1', $sabana->P2_11_1);
                } else {
                    $sabana->P2_11_1_detalle = '';
                }

                if ($sabana->P2_8_1 != ''){
                    $sabana->P2_8_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_8_1', $sabana->P2_8_1);
                } else {
                    $sabana->P2_8_1_detalle = '';
                }

                if ($sabana->P2_13_1 != ''){
                    $sabana->P2_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_13_1', $sabana->P2_13_1);
                } else {
                    $sabana->P2_13_1_detalle = '';
                }

                if ($sabana->P2_14_1 != ''){
                    $sabana->P2_14_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_14_1', $sabana->P2_14_1);
                } else {
                    $sabana->P2_14_1_detalle = '';
                }

                if ($sabana->P2_15_1 != ''){
                    $sabana->P2_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_15_1', $sabana->P2_15_1);
                } else {
                    $sabana->P2_15_1_detalle = '';
                }

                if ($sabana->P2_16_1 != ''){
                    $sabana->P2_16_1_detalle = $this->recuperaRespuestas($respuestas, 'P2_16_1', $sabana->P2_16_1);
                } else {
                    $sabana->P2_16_1_detalle = '';
                }

            }
            else if ($this->pauta == 3){
                $vista = "exports.sabana_lavado";
                $sabana->fecha_tramo = $this->FechaTramo($sabana->fecha); 
                $sabana->horallegada_tramo =  $this->tramoHorario($sabana->horallegada);

                if ($sabana->P3_5_1 != ''){
                    $sabana->P3_5_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_5_1', $sabana->P3_5_1);
                } else {
                    $sabana->P3_5_1_detalle = '';
                }

                

                if ($sabana->P3_13_1 != ''){
                    $sabana->P3_13_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_13_1', $sabana->P3_13_1);
                } else {
                    $sabana->P3_13_1_detalle = '';
                }

                if ($sabana->P3_14_1 != ''){
                    $sabana->P3_14_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_14_1', $sabana->P3_14_1);
                } else {
                    $sabana->P3_14_1_detalle = '';
                }

                if ($sabana->P3_15_1 != ''){
                    $sabana->P3_15_1_detalle = $this->recuperaRespuestas($respuestas, 'P3_15_1', $sabana->P3_15_1);
                } else {
                    $sabana->P3_15_1_detalle = '';
                }

                
            }
        }


        return view($vista, [
            'sabanas' => $sabanas,
            // 'respuestas' => $respuestas
        ]);
    }
}
